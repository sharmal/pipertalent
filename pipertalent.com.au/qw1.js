<script src="https://checkout.stripe.com/checkout.js"></script>
<script>
jQuery(document).ready(function($) {
    var maxLength = 200;




    $('textarea#wpbdp-field-18').keyup(function() {
      checkMaxLength(this.id, maxLength);
    });

    $('.stripe-button-el').hide();
   $('.fusion-toggle-heading').each(function () {
    if ($(this).text() ==' list of inclusions >') {
         $(this).css('font-size', '12px');
    	 $(this).css('font-style', 'italic');
    	 $(this).css('text-decoration', 'underline');
    	 $(this).siblings('div').hide();
    	 $(this).css('margin-left', '0px');
    	 $(this).css('margin-top', '-10px');
    }
});

     setTimeout(function(){
        $('#tabs li:first').removeClass('active');
            $('#tabs li:nth-child(1)').removeClass('active');
        }, 500);

    function validateEmail(sEmail) {
      var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
      if (filter.test(sEmail)) {
          return true;
      }
      else {
        //alert('sssxsxs')
          return false;
      }
    }

    function validate(value){
      if (value === 1) {
        if ($('#input_3').val() == "") {
            e.preventDefault();
            $( "#input_3" ).closest( "li" ).addClass("form-line-error");
            $('#m_options_list').hide();
            $('#m_labels_list').hide();
        }else {
          $( "#input_3" ).closest( "li" ).removeClass("form-line-error");
          //Start-up
          if ($('#input_3').val() == "Start-up") {
            $('#m_options_list').hide();
            $('#m_labels_list').hide();
          }
          else {
            $('#m_options_list').show();
            $('#m_labels_list').show();
          }

        }
      }
      else if (value === 2) {
        if ($('#input_5').val() == "") {
            e.preventDefault();
            $( "#input_5" ).closest( "li" ).addClass("form-line-error");
        }else {
          $( "#input_5" ).closest( "li" ).removeClass("form-line-error");
        }
      }
      else if (value === 3) {
        if ($('#input_9').val() == "") {
            e.preventDefault();
            $( "#input_9" ).closest( "li" ).addClass("form-line-error");
        }else {
          $( "#input_9" ).closest( "li" ).removeClass("form-line-error");
        }
      }
      else if (value === 4) {
        if ($('#input_13').val() == "") {
            e.preventDefault();
            $( "#input_13" ).closest( "li" ).addClass("form-line-error");
        }else {
          $( "#input_13" ).closest( "li" ).removeClass("form-line-error");
        }
      }
      else if (value === 5) {
        if ($('#input_14').val() == "") {
            e.preventDefault();
            $( "#input_14" ).closest( "li" ).addClass("form-line-error");
        }else {
          $( "#input_14" ).closest( "li" ).removeClass("form-line-error");
        }
      }
      else if (value === 6) {
        if ($('#input_12').val() == "") {
            e.preventDefault();
            $( "#input_12" ).closest( "li" ).addClass("form-line-error");
        }else {
          $( "#input_12" ).closest( "li" ).removeClass("form-line-error");
        }
      }
      else if (value === 62) {
        if ($('#input_121').val() == "") {
            e.preventDefault();
            $( "#input_121" ).closest( "li" ).addClass("form-line-error");
        }else {
          $( "#input_121" ).closest( "li" ).removeClass("form-line-error");
        }
      }
      else if (value === 7) {
        if ($('#input_6').val() == "") {
            e.preventDefault();
            $( "#input_6" ).closest( "li" ).addClass("form-line-error");
        }else {
          $( "#input_6" ).closest( "li" ).removeClass("form-line-error");
        }
      }
      else if (value === 8) {
        if ($('#input_6_confirm').val() == "") {
            e.preventDefault();
            $( "#input_6_confirm" ).closest( "li" ).addClass("form-line-error");
        }else {
          $( "#input_6_confirm" ).closest( "li" ).removeClass("form-line-error");
        }
      }
      else if (value === 9) {
        if ($('#cid_payment_input').val() == "") {
            e.preventDefault();
            $( "#cid_payment_input" ).closest( "li" ).addClass("form-line-error");
        }else {
          $( "#cid_payment_input" ).closest( "li" ).removeClass("form-line-error");
        }
      }
      else if (value === 10) {

        if ($('#input_11_01').is(":checked")) {
          $( "#input_11_01" ).closest( "li" ).removeClass("form-line-error");
        }else {
          $( "#input_11_01" ).closest( "li" ).addClass("form-line-error");
        }
      }
      else if (value === 102) {
        if ($('#input_11_02').is(":checked")) {
            $( "#input_11_02" ).closest( "li" ).removeClass("form-line-error");
        }else {
          $( "#input_11_02" ).closest( "li" ).addClass("form-line-error");
        }
      }

    }

    var targetForm = "#form1";
    var formName = "form1";
    var j_result = {};
    var j_result2 = {};
    var nanoIgnore;
    var apiURL = "https://pcojsisq1f.execute-api.ap-southeast-2.amazonaws.com/Prod/";
    var apiKey = {name: 'nanoKey',
    			  value: '1CE8F975-0197-499C-ACC5-F46F77CCE2D2'
    			 };


    var xhr = new XMLHttpRequest();

    $('#submit_33').click(function(e){
      var status_value = 0;
      if ($('#input_3').val() == "") {
          status_value = 1;
          e.preventDefault();
          $( "#input_3" ).closest( "li" ).addClass("form-line-error");
          //return false;
      }else {
        $( "#input_3" ).closest( "li" ).removeClass("form-line-error");
        //Start-up
        if ($('#input_3').val() == "Start-up") {
          $('#m_labels_list').hide();
        }
        else {
          $('#m_labels_list').show();

          if ($('#input_9').val() == "") {
              status_value = 1;
              e.preventDefault();
              $( "#input_9" ).closest( "li" ).addClass("form-line-error");
          }else {
            $( "#input_9" ).closest( "li" ).removeClass("form-line-error");
          }


          if ($('#input_13').val() == "") {
              status_value = 1;
              e.preventDefault();
              $( "#input_13" ).closest( "li" ).addClass("form-line-error");
          }else {
            $( "#input_13" ).closest( "li" ).removeClass("form-line-error");
          }

          if ($('#input_14').val() == "") {
              status_value = 1;
              e.preventDefault();
              $( "#input_14" ).closest( "li" ).addClass("form-line-error");
          }else {
            $( "#input_14" ).closest( "li" ).removeClass("form-line-error");
          }

          if ($('#cid_payment_input').val() == "") {
              status_value = 1;
              e.preventDefault();
              $( "#cid_payment_input" ).closest( "li" ).addClass("form-line-error");
          }else {
            $( "#cid_payment_input" ).closest( "li" ).removeClass("form-line-error");
          }

        }

      }

      if ($('#input_5').val() == "") {
          status_value = 1;
          e.preventDefault();
          $( "#input_5" ).closest( "li" ).addClass("form-line-error");
      }else {
        $( "#input_5" ).closest( "li" ).removeClass("form-line-error");
      }

      if ($('#input_11_01').is(":checked"))
      {
        $( "#input_11_01" ).closest( "li" ).removeClass("form-line-error");
      }
      else {
        status_value = 1;
        $( "#input_11_01" ).closest( "li" ).addClass("form-line-error");
      }

      if ($('#input_11_02').is(":checked"))
      {
        $( "#input_11_02" ).closest( "li" ).removeClass("form-line-error");
      }
      else {
        status_value = 1;
        $( "#input_11_02" ).closest( "li" ).addClass("form-line-error");
      }


      if ($('#input_12').val() == "") {
          status_value = 1;
          e.preventDefault();
          $( "#input_12" ).closest( "li" ).addClass("form-line-error");
      }else {
        $( "#input_12" ).closest( "li" ).removeClass("form-line-error");
      }

      if ($('#input_121').val() == "") {
          status_value = 1;
          e.preventDefault();
          $( "#input_121" ).closest( "li" ).addClass("form-line-error");
      }else {
        $( "#input_121" ).closest( "li" ).removeClass("form-line-error");
      }


      if ($('#input_12').val() == "") {
          status_value = 1;
          e.preventDefault();
          $( "#input_12" ).closest( "li" ).addClass("form-line-error");
      }else {
        $( "#input_12" ).closest( "li" ).removeClass("form-line-error");
      }

      if ($('#input_121').val() == "") {
          status_value = 1;
          e.preventDefault();
          $( "#input_121" ).closest( "li" ).addClass("form-line-error");
      }else {
        $( "#input_121" ).closest( "li" ).removeClass("form-line-error");
      }

      if ($('#input_6').val() == "") {
          status_value = 1;
          e.preventDefault();
          $( "#input_6" ).closest( "li" ).addClass("form-line-error");
      }else {
        var sEmail = $('#input_6').val();
        if ($.trim(sEmail).length == 0) {
          status_value = 1;
            //alert('Please enter valid email address');
            $( "#input_6" ).closest( "li" ).addClass("form-line-error");
            e.preventDefault();
        }

        if (validateEmail(sEmail)) {
          $( "#input_6" ).closest( "li" ).removeClass("form-line-error");
          e.preventDefault();
        }
        else {
          status_value = 1;
          e.preventDefault();
          $( "#input_6" ).closest( "li" ).addClass("form-line-error");
        }
      }

      if ($('#input_6_confirm').val() == "") {
          status_value = 1;
          e.preventDefault();
          $( "#input_6_confirm" ).closest( "li" ).addClass("form-line-error");
      }else {
        $( "#input_6_confirm" ).closest( "li" ).removeClass("form-line-error");
        var Email = $('#input_6').val();
        var sEmail = $('#input_6_confirm').val();
        if (Email != sEmail) {
          status_value = 1;
          $( "#input_6_confirm" ).closest( "li" ).addClass("form-line-error");
        }
        else {
          $( "#input_6_confirm" ).closest( "li" ).removeClass("form-line-error");
        }
      }

      if (status_value ===1) {
        return false;
      }
      else {

        "use strict";
      	e.preventDefault();
      	var formContent = document.getElementById(formName).elements;
      	var result = {};
      	result[apiKey.name] = apiKey.value;
      	for (var i = 0; i < formContent.length; i++){
      		var field = formContent.item(i);
      		if (field.name === "nanoIgnore"){nanoIgnore = field.value;}
      		switch (field.type){
      			case "radio":
      				if (field.checked){
      					result[field.name] = field.value;
      				}
      				break;
      			case "checkbox":
      				if (field.checked){
      					result[field.name] = field.value;
      				}
      				break;
      			case "reset":
      				break;
      			case "submit":
      				break;
      			default:
      			if (field.value !== "") { //don't allow empty fields to be added to the JSON object or it will be malformed
      				result[field.name] = field.value;
      			}
      		}
      	}
      	j_result = JSON.stringify(result);
      	if (nanoIgnore !== ""){
      		//do nothing
      		console.log("honeytrap field filled");
      	}
      	else { //submit the form
          if ($('#cid_payment_input').val() == 'Card') {
            var amount = 00;
            if ($('#input_3').val() == 'Scaleup') {
              amount = 77000;
            }
            else if ($('#input_3').val() == 'Associate') {
              amount = 137500;
            }
            else if ($('#input_3').val() == 'Corporate') {
              amount = 297000;
            }
            else if ($('#input_3').val() == 'Premium') {
              amount = 495000;
            }
            else if ($('#input_3').val() == 'Gold') {
              amount = 990000;
            }

            var handler = StripeCheckout.configure({
              key: 'pk_live_hKQt4ydSdjT16YXBi7Uupg9D',
              image: 'https://stripe.com/img/documentation/checkout/marketplace.png',
              locale: 'auto',
              token: function(token) {
                console.log('token ',token);
                result['stripe_data'] = token;
                j_result2 = JSON.stringify(result);
                console.log(j_result2);
                // You can access the token ID with `token.id`.
                // Get the token ID to your server-side code for use.

                xhr.open("POST", apiURL, true);
            		xhr.setRequestHeader("X-Api-Key","rsYfcLcVxl2INRGxCs7SbayV2dtj0flIa6cl12dB");
            		xhr.send(j_result2);
            		xhr.onload = function(){
            			if (xhr.status === 200){
            				var responseObject = JSON.parse(xhr.responseText);
            				var redirectResult = responseObject.Location;
                    if ($('#cid_payment_input').val() == 'Invoice') {
                      window.location.href = "http://www.futurebusinesscouncil.com/invoice-me-success/";
                    }else if($('#cid_payment_input').val() == 'Card'){
                      window.location.href = "http://www.futurebusinesscouncil.com/success-payment/";
                    }else {
                      window.location.href = "http://www.futurebusinesscouncil.com/invoice-me-success/";
                    }
            			}else {
            			  window.location.href = "http://www.futurebusinesscouncil.com/fail-payment/";
            			}
            		};
              }
            });

            handler.open({
              name: 'Future Business Council',
              description: 'FBC',
              zipCode: true
              //amount: amount
            });
          }
          else {
            console.log(j_result);
            xhr.open("POST", apiURL, true);
        		xhr.setRequestHeader("X-Api-Key","r1NcIct28LaZXSBtZGsWt1Da0Ssp5ZclYuZsEci0");
        		xhr.send(j_result);
        		xhr.onload = function(){
        			if (xhr.status === 200){
        				var responseObject = JSON.parse(xhr.responseText);
        				var redirectResult = responseObject.Location;
                if ($('#cid_payment_input').val() == 'Invoice') {
                  window.location.href = "http://www.futurebusinesscouncil.com/invoice-me-success/";
                }else if($('#cid_payment_input').val() == 'Card'){
                  window.location.href = "http://www.futurebusinesscouncil.com/success-payment/";
                }else {
                  window.location.href = "http://www.futurebusinesscouncil.com/invoice-me-success/";
                }
        			}else {
        			  window.location.href = "http://www.futurebusinesscouncil.com/fail-payment/";
        			}
        		};
          }
      	}

      }


    });

    window.addEventListener('popstate', function() {
      handler.close();
    });

    $('#input_3').change(function() {
      validate(1)
    })

    $('#input_5').change(function() {
      validate(2)
    })

    $('#input_9').change(function() {
      validate(3)
    })

    $('#input_13').change(function() {
      validate(4)
    })

    $('#input_14').change(function() {
      validate(5)
    })

    $('#input_12').change(function() {
      validate(6)
    })

    $('#input_121').change(function() {
      validate(62)
    })

    $('#input_6').change(function() {
      validate(7)
    });

    $('#input_6_confirm').change(function() {
      validate(8)
    })

    $('#cid_payment_input').change(function() {
      validate(9)
    });

    $('#input_11_01').change(function() {
      validate(10)
    });

    $('#input_11_02').change(function() {
      validate(102)
    });

});

function checkMaxLength(textareaID, maxLength){
    $(this).addClass('active');
    var currentLengthInTextarea = jQuery('#'+textareaID).val().length;

    // jQuery(remainingLengthTempId).text(parseInt(maxLength) – parseInt(currentLengthInTextarea));
    //alert(currentLengthInTextarea);
    if (currentLengthInTextarea > (maxLength)) {

     // Trim the field current length over the maxlength.

        jQuery('textarea#wpbdp-field-18').val(jQuery('textarea#wpbdp-field-18').val().slice(0, maxLength));

        //jQuery(remainingLengthTempId).text(0);

    }

}

</script>
