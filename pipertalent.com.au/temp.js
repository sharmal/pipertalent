class PhoneRow extends React.Component {
	numberList(data) {
  	return data.map((val,k) =>{
      	return (
        		<div key={k}>
              <div>{"Type : "+val.type}</div>
              <div>{"Number : "+val.number}</div>
            </div>
        )
      });

  }

  render() {
    return (
    <div>
      {this.numberList(this.props.data)}
     </div>)
     }
}

class PhoneForm extends React.Component {

	 constructor(props) {
    super(props);
    this.state = {
      type: '',
   		 number:'',
    };
  }

	addMobileNumber() {
    if(this.state.type && this.state.number){
    	var data={
        type:this.state.type,
        number:this.state.number
      }

  	//console.log('type',this.props.addNumber);
      this.props.addNumber(data);
    }else{
    	alert('Invalid input');
    }


  }

  handleChange(event) {

    this.setState({type: event.target.value})
  }

  handleChange2(event) {

    this.setState({number: event.target.value})

  }

	render() {
    return (<div>
    <input type="text"  placeholder="Type" name="type" value={this.state.type}
    onChange={this.handleChange.bind(this)}/>
    <input type="text"  placeholder="Mobile Number"  name="type" value={this.state.number}
    onChange={this.handleChange2.bind(this)}/>
             <button onClick={this.addMobileNumber.bind(this)}>Add</button>
          </div>);
  }
}

class App extends React.Component {

	constructor(props) {
    super(props);
    this.state = {
      data: [{ type: 'home', number: '555-555-5555' }],
    };
  }

  newContact(obj) {
  console.log('this.state.data ',this.state.data)
  	var contactList = this.state.data;
  	    contactList.push(obj);
  	    this.setState({data:contactList});
  }



  render() {
    return (<div>
            <PhoneForm addNumber={this.newContact.bind(this)}/>
            {this.state.data &&  <PhoneRow data={this.state.data} />}
          </div>);
  }
}

ReactDOM.render(
  <App />,
  document.getElementById('container')
);
