<?php

/**
 * Handles form generaton and posting for add/edit post in frontend
 *
 * @package WP User Frontend
 */
class WPUF_Render_Form {

    static $meta_key            = 'wpuf_form';
    static $separator           = '| ';
    static $config_id           = '_wpuf_form_id';
    private $form_condition_key = 'wpuf_cond';
    private static $_instance;
    private $field_count = 0;
    public $multiform_start = 0;

    public static function init() {
        if ( !self::$_instance ) {
            self::$_instance = new WPUF_Render_Form();
        }

        return self::$_instance;
    }

    /**
     * Send json error message
     *
     * @param string $error
     */
    function send_error( $error ) {
        echo json_encode( array(
            'success' => false,
            'error'   => $error
        ) );

        die();
    }

    /**
     * Search on multi dimentional array
     *
     * @param array $array
     * @param string $key name of key
     * @param string $value the value to search
     * @return array
     */
    function search( $array, $key, $value ) {
        $results = array();

        if ( is_array( $array ) ) {
            if ( isset( $array[$key] ) && $array[$key] == $value )
                $results[] = $array;

            foreach ($array as $subarray)
                $results = array_merge( $results, $this->search( $subarray, $key, $value ) );
        }

        return $results;
    }

    /**
     * Really simple captcha validation
     *
     * @return void
     */
    function validate_rs_captcha() {
        $rs_captcha_input = isset( $_POST['rs_captcha'] ) ? $_POST['rs_captcha'] : '';
        $rs_captcha_file  = isset( $_POST['rs_captcha_val'] ) ? $_POST['rs_captcha_val'] : '';

        if ( class_exists( 'ReallySimpleCaptcha' ) ) {
            $captcha_instance = new ReallySimpleCaptcha();

            if ( !$captcha_instance->check( $rs_captcha_file, $rs_captcha_input ) ) {

                $this->send_error( __( 'Really Simple Captcha validation failed', 'wpuf' ) );
            } else {
                // validation success, remove the files
                $captcha_instance->remove( $rs_captcha_file );
            }
        }
    }

    /**
     * reCaptcha Validation
     *
     * @return void
     */
    function validate_re_captcha( $no_captcha = '', $invisible = '' ) {
        // need to check if invisible reCaptcha need library or we can do it here.
        // ref: https://shareurcodes.com/blog/google%20invisible%20recaptcha%20integration%20with%20php
        $site_key        = wpuf_get_option( 'recaptcha_public', 'wpuf_general' );
        $private_key     = wpuf_get_option( 'recaptcha_private', 'wpuf_general' );
        if ( $no_captcha == 1 && 0 == $invisible ) {

            $response = null;
            $reCaptcha = new ReCaptcha($private_key);

            $resp = $reCaptcha->verifyResponse(
                $_SERVER["REMOTE_ADDR"],
                $_POST["g-recaptcha-response"]
            );

            if ( !$resp->success ) {
                $this->send_error( __( 'noCaptcha reCAPTCHA validation failed', 'wpuf' ) );
            }

        } elseif ( $no_captcha == 0 && 0 == $invisible  ) {

            $recap_challenge = isset( $_POST['recaptcha_challenge_field'] ) ? $_POST['recaptcha_challenge_field'] : '';
            $recap_response  = isset( $_POST['recaptcha_response_field'] ) ? $_POST['recaptcha_response_field'] : '';

            $resp            = recaptcha_check_answer( $private_key, $_SERVER["REMOTE_ADDR"], $recap_challenge, $recap_response );

            if ( !$resp->is_valid ) {
                $this->send_error( __( 'reCAPTCHA validation failed', 'wpuf' ) );
            }

        } elseif ( $no_captcha == 0 && 1 == $invisible ) {

            $response  = null;
            $recaptcha = $_POST['g-recaptcha-response'];
            $object    = new Invisible_Recaptcha( $site_key , $private_key );

            $response  = $object->verifyResponse( $recaptcha );

            if ( isset( $response['success'] ) and $response['success'] != true) {
                $this->send_error( __( 'Invisible reCAPTCHA validation failed', 'wpuf' ) );
            }
        }

    }

    /**
     * Guess a suitable username for registration based on email address
     * @param string $email email address
     * @return string username
     */
    function guess_username( $email ) {
        // username from email address
        $username = sanitize_user( substr( $email, 0, strpos( $email, '@' ) ) );

        if ( !username_exists( $username ) ) {
            return $username;
        }

        // try to add some random number in username
        // and may be we got our username
        $username .= rand( 1, 199 );
        if ( !username_exists( $username ) ) {
            return $username;
        }
    }

    /**
     * Get input meta fields separated as post vars, taxonomy and meta vars
     *
     * @param int $form_id form id
     * @return array
     */
    public static function get_input_fields( $form_id ) {
        $form_vars    = wpuf_get_form_fields( $form_id );

        $ignore_lists = array('section_break', 'html');
        $post_vars    = $meta_vars = $taxonomy_vars = array();

        foreach ($form_vars as $key => $value) {

            // ignore section break and HTML input type
            if ( in_array( $value['input_type'], $ignore_lists ) ) {
                continue;
            }

            //separate the post and custom fields
            if ( isset( $value['is_meta'] ) && $value['is_meta'] == 'yes' ) {
                $meta_vars[] = $value;
                continue;
            }

            if ( $value['input_type'] == 'taxonomy' ) {

                // don't add "category"
                if ( $value['name'] == 'category' ) {
                    continue;
                }

                $taxonomy_vars[] = $value;
            } else {
                $post_vars[] = $value;
            }
        }

        return array($post_vars, $taxonomy_vars, $meta_vars);
    }

    public static function prepare_meta_fields( $meta_vars ) {
        // loop through custom fields
        // skip files, put in a key => value paired array for later executation
        // process repeatable fields separately
        // if the input is array type, implode with separator in a field

        $files          = array();
        $meta_key_value = array();
        $multi_repeated = array(); //multi repeated fields will in sotre duplicated meta key

        foreach ($meta_vars as $key => $value) {

            switch ( $value['input_type'] ) {

                // put files in a separate array, we'll process it later
                case 'file_upload':
                case 'image_upload':

                    $files[] = array(
                        'name'  => $value['name'],
                        'value' => isset( $_POST['wpuf_files'][$value['name']] ) ? $_POST['wpuf_files'][$value['name']] : array(),
                        'count' => $value['count']
                    );
                    break;

                case 'repeat':

                    // if it is a multi column repeat field
                    if ( isset( $value['multiple'] ) && $value['multiple'] == 'true' ) {

                        // if there's any items in the array, process it
                        if ( $_POST[$value['name']] ) {

                            $ref_arr = array();
                            $cols    = count( $value['columns'] );
                            $first   = array_shift( array_values( $_POST[$value['name']] ) ); //first element
                            $rows    = count( $first );

                            // loop through columns
                            for ($i = 0; $i < $rows; $i++) {

                                // loop through the rows and store in a temp array
                                $temp = array();
                                for ($j = 0; $j < $cols; $j++) {

                                    $temp[] = $_POST[$value['name']][$j][$i];
                                }

                                // store all fields in a row with self::$separator separated
                                $ref_arr[] = implode( self::$separator, $temp );
                            }

                            // now, if we found anything in $ref_arr, store to $multi_repeated
                            if ( $ref_arr ) {
                                $multi_repeated[$value['name']] = array_slice( $ref_arr, 0, $rows );
                            }
                        }
                    } else {
                        $meta_key_value[$value['name']] = implode( self::$separator, $_POST[$value['name']] );
                    }

                    break;

                case 'address':

                    if ( isset( $_POST[ $value['name'] ] ) && is_array( $_POST[ $value['name'] ] ) ) {
                        foreach ( $_POST[ $value['name'] ] as $address_field => $field_value ) {
                            $meta_key_value[ $value['name'] ][ $address_field ] = sanitize_text_field( $field_value );
                        }
                    }

                    break;

                case 'text':
                case 'email':
                case 'number':
                case 'date':

                    $meta_key_value[$value['name']] = sanitize_text_field( trim( $_POST[$value['name']] ) );

                    break;

                case 'textarea':

                    $meta_key_value[$value['name']] = wp_kses_post( $_POST[$value['name']] );

                    break;

                default:
                    // if it's an array, implode with this->separator
                    if ( is_array( $_POST[$value['name']] ) ) {

                        if ( $value['input_type'] == 'address' ) {
                            $meta_key_value[$value['name']] = $_POST[$value['name']];
                        } else {
                            $meta_key_value[$value['name']] = implode( self::$separator, $_POST[$value['name']] );
                        }
                    } else {
                        $meta_key_value[$value['name']] = trim( $_POST[$value['name']] );
                    }

                    break;
            }

        } //end foreach

        return array($meta_key_value, $multi_repeated, $files);
    }

    function guest_fields( $form_settings ) {
        ?>
        <li class="el-name">
            <div class="wpuf-label">
                <label><?php echo $form_settings['name_label']; ?> <span class="required">*</span></label>
            </div>

            <div class="wpuf-fields">
                <input type="text" required="required" data-required="yes" data-type="text" name="guest_name" value="" size="40">
            </div>
        </li>

        <li class="el-email">
            <div class="wpuf-label">
                <label><?php echo $form_settings['email_label']; ?> <span class="required">*</span></label>
            </div>

            <div class="wpuf-fields">
                <input type="email" required="required" data-required="yes" data-type="email" name="guest_email" value="" size="40">
            </div>
        </li>
        <?php
    }

    /**
     * Handles the add post shortcode
     *
     * @param int $form_id
     * @param int $post_id
     */
    function render_form( $form_id, $post_id = NULL ) {

        $form_status = get_post_status( $form_id );


        if ( ! $form_status ) {
            echo '<div class="wpuf-message">' . __( 'Your selected form is no longer available.', 'wpuf' ) . '</div>';
            return;
        }

        if ( $form_status != 'publish' ) {
            echo '<div class="wpuf-message">' . __( "Please make sure you've published your form.", 'wpuf' ) . '</div>';
            return;
        }

        $form_vars      = wpuf_get_form_fields( $form_id );
        $form_settings  = wpuf_get_form_settings( $form_id );
        $label_position = isset( $form_settings['label_position'] ) ? $form_settings['label_position'] : 'left';
        $layout         = isset( $form_settings['form_layout'] ) ? $form_settings['form_layout'] : 'layout1';

        if ( !empty( $layout ) ) {
            wp_enqueue_style( 'wpuf-' . $layout );
        }

        if ( ! is_user_logged_in() && $form_settings['guest_post'] != 'true' ) {
            echo '<div class="wpuf-message">' . $form_settings['message_restrict'] . '</div>';
            return;
        }

        if ($form_id === '1225') {
          //echo do_shortcode( '[wpuf_time_sheet_list]' );
          ?><meta name="viewport" content="width=device-width, initial-scale=1">
          <meta http-equiv='cache-control' content='no-cache'>
          <meta http-equiv='expires' content='0'>
          <meta http-equiv='pragma' content='no-cache'>
          <link rel="stylesheet" href="<?php echo plugins_url( 'assets/css/bootstrap.css', dirname( __FILE__ ) ); ?>">
          <link rel="stylesheet" href="<?php echo plugins_url( 'assets/css/timepicki.css', dirname( __FILE__ ) ); ?>">
          <script src="<?php echo plugins_url( 'assets/js/timepicki.js', dirname( __FILE__ ) ); ?>"></script>
          <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.5/jspdf.debug.js" integrity="sha384-CchuzHs077vGtfhGYl9Qtc7Vx64rXBXdIAZIPbItbNyWIRTdG0oYAqki3Ry13Yzu" crossorigin="anonymous"></script>
          <link rel="stylesheet" href="<?php echo plugins_url( 'assets/datepicker/bootstrap-datetimepicker.min.css', dirname( __FILE__ ) ); ?>">
          <script src="<?php echo plugins_url( 'assets/datepicker/bootstrap/js/bootstrap.min.js', dirname( __FILE__ ) ); ?>"></script>
          <script src="<?php echo plugins_url( 'assets/datepicker/bootstrap-datetimepicker.js', dirname( __FILE__ ) ); ?>"></script>
          <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
          <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css"></style>
          <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
          <!-- <script type="text/javascript" src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script> -->

            <script type="text/javascript">
                ;(function($) {

                    $(document).ready( function(){
                      var x_state = 0;
                      $('.approver_comment').hide();
                      $('#myTable_paginate').css('width: 60%;')
                      $('.getData').click(function(){
                        var count =0;
                        var status = null;
                        var email_address = null;
                        var con_email_address = null;
                        var update_state = 0;
                        var sum_array=[];
                        //$('#re_submit').show();
                        $('#mdTable').html("");
                        $('.modal-backdrop').css('z-index: -1');

                        $('#mdTable').html("");
                        $('#mdTable').html($(this).closest('div' ).find('div').html());

                        $(this).closest('tr').find('td:eq(8)').each(function() {
                          status = $(this).text();
                        });

                        $(this).closest('tr').find('td:eq(9)').each(function() {
                          //$(this).find($(this).text().hide());
                          var status1 = $(this).text();
                          $('#mdTable').append('<p>'+status1+'</p>');

                        });

                        $(this).closest('tr').find('td:eq(3)').each(function() {
                          con_email_address = $(this).text();
                        });

                        $(this).closest('tr').find('td:not(:first-child)').each(function() {
                          count++;
                          var textval = $(this).text();
                          var Emial_add_12 = 0;
                          // $(this).closest('tr').find('td:eq(5)').each(function() {
                          //   Emial_add_12= $(this).find('#temp_email2').val();

                          // });
                          //$('#mdTable').append($('#temp_comment2').val());
                          if (status == 'Rejected' && con_email_address == $('#temp_email2').val()) {
                            update_state=1;
                            //commondatepcker();
                            $('#mdTable').find('#BTN_ADD').show();
                            $('#re_submit').show();
                            if (count < 5) {
                              $('#input_model_'+count).val(textval).prop("readonly", true);
                            }else {
                              $('#input_model_'+count).val(textval).prop("readonly", false);
                            }

                          }else {
                            $('#re_submit').hide();
                            $('#mdTable').find('#BTN_ADD').hide();
                            //$(this).find('#BTN_ADD').hide();
                            $('#input_model_'+count).val(textval).prop("readonly", true);
                          }

                        });

                        $('#record_id').val($(this).closest('tr').attr('id'));

                        $(this).closest('tr').find('td:eq(7)').each(function() {
                          email_address = $(this).text();
                        });

                        if ($('#temp_email2').val() == email_address && status == 'Pending' ) {
                          $('#Approve').show();
                          $('#Reject').show();
                          $('.approver_comment').show();
                          //$('#re_submit').hide();
                        }
                        else {
                          $('#Approve').hide();
                          $('#Reject').hide();
                          //$('#re_submit').hide();
                        }
                        var time = 0;
                        if (update_state ==0 ) {
                          $('#err_msg').hide();
                          $('#mdTable #timeSheetTableBody tr').find('td:eq(0)').each(function() {
                            time = $(this).find(".dtp_input2").val();
                            $(this).find('div#temp').empty();
                            $(this).append('<div id="temp" style="margin-bottom: 6px;" class="form-group"><div style="display: ruby; white-space: nowrap; width: 100%">'+
                            '<input style="width: 100%;" class="inputText" type="text" value="'+time+'" readonly>'+
                                '<span>'+
                                '</span>'+
                              '</div>'+
                            '</div>');

                            $(this).find('.form_date').hide();
                          });

                          $('#mdTable #timeSheetTableBody tr').find('td:eq(1)').each(function() {
                            $(this).find('#temp').empty();
                            $(this).append('<div  id="temp" style="margin-bottom: 6px;" class="form-group"><div style="display: ruby; white-space: nowrap; width: 100%">'+
                            '<input style="width: 100%;" class="inputText" type="text" value="'+$(this).find(".timeDisplay").val()+'" readonly>'+
                                '<span>'+
                                '</span>'+
                              '</div>'+
                            '</div>');
                            $(this).find('.bootstrap-timepicker').hide();
                          })

                          $('#mdTable #timeSheetTableBody tr').find('td:eq(2)').each(function() {
                            $(this).find('#temp').empty();
                            $(this).append('<div  id="temp" style="margin-bottom: 6px;" class="form-group"><div style="display: ruby; white-space: nowrap; width: 100%">'+
                            '<input style="width: 100%;" class="inputText" type="text" value="'+$(this).find(".timeDisplay").val()+'" readonly>'+
                                '<span>'+
                                '</span>'+
                              '</div>'+
                            '</div>');
                            $(this).find('.bootstrap-timepicker').hide();
                          })

                          $('#mdTable #timeSheetTableBody tr').find('td:eq(3)').each(function() {
                            $(this).find('#temp').empty();
                            $(this).append('<div id="temp" style="margin-bottom: 6px;" class="form-group"><div style="display: ruby; white-space: nowrap; width: 100%">'+
                            '<input style="width: 100%;" class="inputText" type="text" value="'+$(this).find(".timeDisplay").val()+'" readonly>'+
                                '<span>'+
                                '</span>'+
                              '</div>'+
                            '</div>');
                            $(this).find('.bootstrap-timepicker').hide();
                          });

                          $('#mdTable #timeSheetTableBody2 tr').find('td:eq(4)').each(function() {
                            $(this).find("#txt_total").val($(this).find(".txt_total").val())
                          });

                          $('#mdTable #timeSheetTableBody tr').find('td:eq(4)').each(function() {
                            $(this).find('#temp').empty();
                            $(this).find('.form-group').hide();
                            //$(this).find('#BTN_ADD').hide();
                            if (update_state ==0 ) {
                              $(this).append('<div id="temp" style="margin-bottom: 0px;" class="form-group2"><div style="display: ruby; white-space: nowrap; width: 100%">'+
                              '<input style="margin-top: 9px;" class="inputText" type="text" value="'+$(this).find(".timeDisplay").val()+'" readonly>'+
                                  '<span>'+
                                  '</span>'+
                                '</div>'+
                              '</div>');

                              //$('#BTN_ADD').hide();
                            }else {

                              $(this).append('<div id="temp" style="margin-bottom: 0px;" class="form-group2"><div style="display: ruby; white-space: nowrap; width: 100%">'+
                              '<input style="margin-top: 9px;" class="inputText" type="text" value="'+$(this).find(".timeDisplay").val()+'" readonly>'+
                                  '<span style="margin-left:10px;cursor: pointer;" class="del2" ><i class="glyphicon glyphicon-remove-circl2e"></i></span>'+
                                '</div>'+
                              '</div>');
                              //$('#BTN_ADD').hide();
                            }

                            $(".del2").bind('click', function() {

                               $(this).closest('tr').remove();
                               var count = $('#mdTable #timeSheetTableBody').children('tr').length;
                               if (count == 1) {
                                 $('.del2').hide();
                               }
                               getTotal();

                             });
                           });

                        }
                        else {
                          $('#err_msg').hide();
                          $('#mdTable #timeSheetTableBody tr').find('td:eq(0)').each(function() {
                            time = $(this).find(".dtp_input2").val();
                            $(this).find('div#temp').empty();
                            $(this).prepend('<div id="temp" style="margin-bottom: 6px;" class="form-group"><div style="display: ruby; white-space: nowrap; width: 100%">'+
                            '<input style="width: 100%;" class="inputText" type="text" value="'+time+'" readonly>'+
                                '<span>'+
                                '</span>'+
                              '</div>'+
                            '</div>');

                            $(this).find('.form_date').hide();
                            //$(this).find('.form_date').val('2018-03-14');
                          });

                          $('#mdTable #timeSheetTableBody tr').find('td:eq(1)').each(function() {
                            $(this).find('#temp').empty();
                            $(this).find('.bootstrap-timepicker').show();


                            //$(this).find('.timepicker3').timepicker('setTime', newTime);
                            $(this).closest('#timeSheetTableBody tr').find('td:eq(1)').each(function() {
                              var newTime= $(this).find('.timeDisplay').val();
                              $(this).find('.timepicker1').timepicker('setTime', newTime);
                            })
                          })

                          $('#mdTable #timeSheetTableBody tr').find('td:eq(2)').each(function() {
                            $(this).find('#temp').empty();
                            $(this).find('.bootstrap-timepicker').show();
                            $(this).closest('#timeSheetTableBody tr').find('td:eq(2)').each(function() {
                              var newTime= $(this).find('.timeDisplay').val();
                              $('.timepicker2').timepicker({
                                showMeridian:false,
                                defaultTime: '0.00'
                              });
                              $(this).find('.timepicker2').timepicker('setTime', newTime);
                            })
                          })

                          $('#mdTable #timeSheetTableBody tr').find('td:eq(3)').each(function() {
                            $(this).find('#temp').empty();
                            $(this).find('.bootstrap-timepicker').show();
                            $(this).closest('#timeSheetTableBody tr').find('td:eq(3)').each(function() {
                              var newTime= $(this).find('.timeDisplay').val();
                              $(this).find('.timepicker3').timepicker('setTime', newTime);
                            })
                          });

                          $('#mdTable #timeSheetTableBody2 tr').find('td:eq(4)').each(function() {
                            $(this).find("#txt_total").val($(this).find(".txt_total").val())
                          });

                          $('#mdTable #timeSheetTableBody tr').find('td:eq(4)').each(function() {
                            $(this).find('#temp').empty();

                            $(this).find('.form-group').show();
                            $(this).find('#BTN_ADD').show();

                            $(this).closest('#timeSheetTableBody tr').find('td:eq(4)').each(function() {
                               var newTime = $(this).find('.timeDisplay').val();
                              $(this).find('.day_sum').val(newTime);
                            });
                            //$('#BTN_ADD').show();
                            $(".del2").bind('click', function() {

                               $(this).closest('tr').remove();
                               var count = $('#mdTable #timeSheetTableBody').children('tr').length;
                               if (count == 1) {
                                 $('.del2').hide();
                               }
                               getTotal();

                             });

                             var count = $(this).closest('#timeSheetTableBody').children('tr').length;
                             if (count == 1) {
                               $('.del').hide();
                             }else {
                               $('.del').show();
                             }
                           });
                           commondatepcker();
                        }

                       $('#mdTable').find('#BTN_ADD').click(function() {
                         var row_count = $('#mdTable #timeSheetTableBody').children('tr').length;
                         if (row_count < 7) {
                           $('#mdTable #timeSheetTableBody').append(appendTable('remove'));
                           commondatepcker();

                           $(".del").bind('click', function() {

                              $(this).closest('tr').remove();
                              var count = $('#mdTable #timeSheetTableBody').children('tr').length;
                              if (count == 1) {
                                $('.del').hide();
                              }
                              //row_count = count-1;
                              getTotal();

                          });
                          var specifiedElement = document.getElementById('mdTable');
                          document.addEventListener('click', function(event) {
                              var isClickInside = specifiedElement.contains(event.target);
                              if (isClickInside) {
                                //console.log('You clicked inside')
                              }
                              else {
                                validateTable();
                              }
                          });
                         }

                       });

                       $(".del").bind('click', function() {

                          $(this).closest('tr').remove();
                          var count = $('#mdTable #timeSheetTableBody').children('tr').length;
                          //row_count = count-1;
                          getTotal();

                       });

                       var specifiedElement = document.getElementById('mdTable');
                       document.addEventListener('click', function(event) {
                           var isClickInside = specifiedElement.contains(event.target);
                           if (isClickInside) {
                             //console.log('You clicked inside')
                           }
                           else {
                             validateTable();
                           }
                       });
                      });




                      $('#Approve').click(function(){
                        $('#record_action').val('1');
                        $('#myModal').modal('hide');
                      });
                      $('#Reject').click(function(){
                        $('#record_action').val('2');
                        $('#myModal').modal('hide');
                      });
                      $('#re_submit').click(function(){

                        $('#timeSheetTableBody tr').find('td:eq(0)').each(function() {
                          time = $(this).find(".dtp_input2").val();
                          $(this).find('div#temp').empty();
                          $(this).find('.form_date').show();
                          //$(this).find('.form_date').val('2018-03-14');
                        });
                        $('#record_action2').val($("#mdTable").html());
                        $('#record_action').val('3');
                        $('#myModal').modal('hide');

                      });
                      $('#Print').click(function(){
                        printElement(document.getElementById("printThis"), true, "<hr />");
                      });
                      $('#mdTable').append($('#timeSheetData').val())
                      //Print/Approve/Reject

                      function appendTable(para=1) {
                        var row_count = $('#mdTable #timeSheetTableBody').children('tr').length + 1;
                        return(
                          "<tr>"+
                            "<td style='border:0px;'>"+
                              "<div class='form-group'>"+
                                  "<div class='input-group date form_date ' data-date-format='dd-mm-yyyy' data-link-field='dtp_input2"+row_count+"' data-link-format='yyyy-mm-dd'>"+
                                      "<input class='form-control' size='16' type='text' value='' readonly>"+
                                      "<span class='input-group-addon'><span class='glyphicon glyphicon-calendar'></span></span>"+
                                  "</div>"+
                                  "<input class='dtp_input2' type='hidden' id='dtp_input2"+row_count+"' value='' />"+
                              "</div>"+
                            "</td>"+
                            "<td style='border:0px;'>"+
                              "<div class='form-group'>"+
                                  "<div style='width: 100%' class='input-group bootstrap-timepicker timepicker'>"+
                                      "<input   type='text' class='timepicker1 form-control input-small'>"+
                                      "<span class='input-group-addon'><i class='glyphicon glyphicon-time'></i></span>"+
                                  "</div>"+
                                  "<input  type='hidden' class='timeDisplay'>"+
                              "</div>"+
                            "</td>"+
                            "<td style='border:0px;'>"+
                              "<div class='form-group'>"+
                                  "<div  style='width: 100%' class='input-group bootstrap-timepicker timepicker'>"+
                                      "<input  type='text' class='timepicker2 form-control input-small'>"+
                                      "<span class='input-group-addon'><i class='glyphicon glyphicon-time'></i></span>"+
                                  "</div>"+
                                  "<input  type='hidden' class='timeDisplay' value='0'>"+
                              "</div>"+
                            "</td>"+
                            "<td style='border:0px;'>"+
                              "<div class='form-group'>"+
                                  "<div  style='width: 100%' class='input-group bootstrap-timepicker timepicker'>"+
                                      "<input  type='text' class='timepicker3 form-control input-small'>"+
                                      "<span class='input-group-addon'><i class='glyphicon glyphicon-time'></i></span>"+
                                  "</div>"+
                                  "<input  type='hidden' class='timeDisplay'>"+
                              "</div>"+
                            "</td>"+
                            "<td style='border:0px;'>"+
                              "<div class='form-group'>"+
                                  "<div style='display: ruby; white-space: nowrap; width: 100%' >"+
                                      "<input  type='text' class='day_sum inputText' readonly>"+removeButton(para)+
                                  "</div>"+
                                  "<input type='hidden' class='timeDisplay'>"+
                              "</div>"+
                            "</td>"+
                          "</tr>"
                        );
                      }

                      function removeButton(obj) {
                        if (obj == 1) {
                          return("<span></span>");
                        }
                        else {
                          return("<span style='margin-left:10px;cursor: pointer;' class='del' ><i class='glyphicon glyphicon-remove-circle'></i></span>");
                        }
                      }

                      function printElement(elem, append, delimiter) {
                          var domClone = elem.cloneNode(true);
                          //printThis
                          var $printSection = document.getElementById("printSection");

                          if (!$printSection) {
                              var $printSection = document.createElement("div");
                              $printSection.id = "printSection";
                              document.body.appendChild($printSection);
                          }

                          $printSection.innerHTML = "";
                          $printSection.appendChild(domClone);
                          window.print();
                          // var domClone = elem.cloneNode(true);
                          //
                          // var $printSection = document.getElementById("printSection");
                          //
                          // if (!$printSection) {
                          //     $printSection = document.createElement("div");
                          //     $printSection.id = "printSection";
                          //     document.body.appendChild($printSection);
                          // }
                          //
                          // if (append !== true) {
                          //     $printSection.innerHTML = "";
                          // }
                          //
                          // else if (append === true) {
                          //     if (typeof (delimiter) === "string") {
                          //         $printSection.innerHTML += delimiter;
                          //     }
                          //     else if (typeof (delimiter) === "object") {
                          //         $printSection.appendChlid(delimiter);
                          //     }
                          // }
                          //
                          // $printSection.appendChild(domClone);
                          // window.print();
                      }

                      function commondatepcker(){

                        $('.form_date').datetimepicker({
                          weekStart: 1,
                          todayBtn:  1,
                          autoclose: 1,
                          todayHighlight: 1,
                          startView: 2,
                          minView: 2,
                          forceParse: 0,
                        });

                        $('.datetimepicker').click(function() {
                          validateTable();
                        });

                        // $(".form_date").on("dp.change", ".date", function(e){
                        // 	//works
                        //   console.log('d3d33333');
                        // });


                        $('.timepicker1').timepicker({
                          defaultTime: false
                        }).on('changeTime.timepicker', function(e) {
                            $(this).closest('tr').find('td:eq(3)').each(function() {
                              $(this).find('.timepicker3').timepicker('setTime', $('.timepicker1').val());
                            });
                            validateTable();
                          });

                        $('.timepicker2').timepicker({
                          showMeridian:false,
                          defaultTime: '0.00'
                        }).on('changeTime.timepicker', function(e) {
                          $(this).closest('tr').find('td:eq(3)').each(function() {
                            $(this).find('.timepicker3').timepicker('setTime', $('.timepicker1').val());
                            //$('.timepicker3').timepicker('setTime', $('.timepicker1').val());
                            //return false;
                          });
                          validateTable();

                        });

                        $('.timepicker3').timepicker({
                          defaultTime: false
                        }).on('changeTime.timepicker', function(e) {
                            var h= e.time.hours;
                            var m= e.time.minutes;
                            var mer= e.time.meridian;
                            var time1 = 0;
                            var Time = 0;
                            $(this).closest('tr').find('td:eq(1)').each(function() {
                              time1 = $(this).find('.timepicker1').val();
                            });

                            var timeStringArray = time1.split(':');
                            if (timeStringArray.length > 1) {
                               Time =  timeStringArray[1].split(' ');
                            }



                            if (Time[1] == 'AM') {
                              if (mer == 'PM') {
                                if(h != 12){
                                  h= h+12;
                                }

                              }
                              else if(h == 12){
                                h= h-12;
                              }
                            }else {
                              if(h == 12){
                                h= h-12;
                              }
                            }


                            //convert hours into minutes
                            m+=h*60;
                            //10:15 = 10h*60m + 15m = 615 min

                            $(this).closest('tr').find('td:eq(1)').each(function() {
                               var status = $(this).find('.timeDisplay').val();
                               var timeStringArray = status.split(':');
                               if (timeStringArray.length > 1) {


                                 var Time2 =  timeStringArray[1].split(' ');
                                 var timeString = parseInt(timeStringArray[0]) * 60 ;

                                 timeString += parseInt(Time2[0]);


                                 if (Time2[1] == 'AM' && timeStringArray[0] == 12) {
                                   timeString= (timeString % 60);
                                 }

                                 if (mer==Time2[1] && Time2[1] == 'PM' && timeStringArray[0] == 12) {
                                   timeString= (timeString % 60);
                                 }

                                 if(mer==Time2[1] && m<timeString){
                                   $(this).closest('tr').find('td:eq(3)').each(function() {
                                     $(this).find('.timepicker3').timepicker('setTime', status);
                                   })
                                 }else {
                                   var minute_time = (m - timeString) % 60;
                                   var hour_time = Math.floor((m - timeString) / 60);

                                   $(this).closest('tr').find('td:eq(4)').each(function() {
                                     var new_hours2 = 0;
                                     var new_min2 = 0;
                                     $(this).closest('tr').find('td:eq(2)').each(function() {

                                       var status2 = $(this).find('.timepicker2').val();
                                       var timeStringArray2 = status2.split(':');
                                       new_hours2 =  parseInt(timeStringArray2[0]) ;
                                       new_min2 =  parseInt(timeStringArray2[1]) ;

                                     });


                                     if (new_hours2 > hour_time) {
                                       $(this).find('.day_sum').val('0.00');
                                       $(this).find('.timeDisplay').val('0.00');
                                     }
                                     else if (mer == 'AM' && Time[1] == 'PM') {
                                       $(this).find('.day_sum').val('0.00');
                                       $(this).find('.timeDisplay').val('0.00');
                                     }
                                     else {
                                       var h_time = hour_time - new_hours2;
                                       var m_time = 0;
                                       if (new_min2 > minute_time) {
                                         if (h_time > 0) {
                                           m_time = (parseInt(minute_time) + 60) - new_min2;
                                           h_time =h_time - 1;
                                           //var new_time = h_time+'.'+m_time;

                                           var m_time_1 = 0;
                                           if (m_time.toString().length == 1) {
                                             m_time_1 = '0'+m_time
                                           }else {
                                             m_time_1 = m_time;
                                           }
                                           var new_time = h_time+'.'+m_time_1;
                                           var newTime2 = (parseInt(h_time)*60) + parseInt(m_time_1);
                                           var day_time = (parseFloat(newTime2).toFixed(2))/450;
                                           if ($('#input_model_4').val() == 'Daily') {
                                             $(this).find('.day_sum').val(day_time.toFixed(2));
                                             $(this).find('.timeDisplay').val(day_time.toFixed(2));
                                           }else {
                                             $(this).find('.day_sum').val(new_time);
                                             $(this).find('.timeDisplay').val(new_time);
                                           }





                                           // var day_time = (parseFloat(new_time).toFixed(2))/7.5
                                           // //parseFloat(amount).toFixed(1);
                                           // if ($('#input_model_4').val() == 'Daily') {
                                           //   $(this).find('.day_sum').val(day_time.toFixed(2));
                                           //   $(this).find('.timeDisplay').val(day_time.toFixed(2));
                                           // }else {
                                           //   $(this).find('.day_sum').val(new_time);
                                           //   $(this).find('.timeDisplay').val(new_time);
                                           // }

                                         }
                                         else {
                                           $(this).find('.day_sum').val('0.00');
                                           $(this).find('.timeDisplay').val('0.00');
                                         }

                                       }else {
                                         m_time = parseInt(minute_time)  - new_min2;
                                         var m_time_1 = 0;
                                         if (m_time.toString().length == 1) {
                                           m_time_1 = '0'+m_time
                                         }else {
                                           m_time_1 = m_time;
                                         }
                                         var new_time = h_time+'.'+m_time_1;
                                         //var new_time = h_time+'.'+m_time;
                                         var newTime2 = (parseInt(h_time)*60) + parseInt(m_time_1);
                                         var day_time = (parseFloat(newTime2).toFixed(2))/450;
                                         if ($('#input_model_4').val() == 'Daily') {
                                           $(this).find('.day_sum').val(day_time.toFixed(2));
                                           $(this).find('.timeDisplay').val(day_time.toFixed(2));
                                         }else {
                                           $(this).find('.day_sum').val(new_time);
                                           $(this).find('.timeDisplay').val(new_time);
                                         }
                                       }
                                     }

                                   });

                                 }
                               }
                            });

                            getTotal();
                            validateTable();

                          });

                        $('.timepicker1').change(function() {
                          $(this).parent().siblings(".timeDisplay").val($(this).val()) ;
                          validateTable();
                        });

                        $('.timepicker2').change(function() {
                          $(this).parent().siblings(".timeDisplay").val($(this).val()) ;
                          validateTable();
                        });

                        $('.timepicker3').change(function() {
                          $(this).parent().siblings(".timeDisplay").val($(this).val()) ;
                          validateTable();
                        });

                        validateTable();
                      }

                      function getTotal() {
                        var sum = 0;
                        var sum2 = 0;

                        $("#mdTable tr").each(function() {
                          $(this).find("td:nth-child(5) .day_sum").map(function () {
                            var value = $(this).val();

                            // add only if the value is number
                            if(!isNaN(value) && value.length != 0) {
                                var full_time = value.split('.');
                                var hr = full_time[0];
                                var min = full_time[1];
                                var fullmin = (parseInt(hr)*60)+parseInt(min);
                                sum += parseInt(fullmin);
                                sum2 = parseFloat(sum2)+parseFloat(value);
                            }
                          });
                        });

                        var full_hr = parseInt(sum/60);
                        var full_min = sum%60;
                        var flat_min =0;
                        if (full_min.toString().length == 1) {
                          flat_min = '0'+full_min
                        }
                        else {
                          flat_min = full_min;
                        }

                        if ($('#input_model_4').val() == 'Daily') {
                          //var day_time = (parseFloat(sum).toFixed(2))/7.5
                          $("#mdTable tr").each(function() {
                            $(this).find('#txt_total').val(sum2.toFixed(2));
                            $(this).find('.txt_total').val(sum2.toFixed(2));
                          });
                          // $('#txt_total').val(sum2.toFixed(2));
                          // $('.txt_total').val(sum2.toFixed(2));
                        }else {
                          $("#mdTable tr").each(function() {
                            $(this).find('#txt_total').val(full_hr+'.'+flat_min);
                            $(this).find('.txt_total').val(full_hr+'.'+flat_min);
                          });
                          // $('#txt_total').val(full_hr+'.'+flat_min);
                          // $('.txt_total').val(full_hr+'.'+flat_min);
                        }

                        // $('#txt_total').val(full_hr+'.'+flat_min);
                        // $('.txt_total').val(full_hr+'.'+flat_min);

                        //return sum;
                      }

                      function validateTable(){
                        var disableState =false;
                        //var disableState2 =false;
                        // $("#mdTable #timeSheetTable").find("input:hidden").map(function () {
                        //
                        //     if (!$(this).val()) {
                        //       disableState =true;
                        //     }
                        // });



                        $("#mdTable tr").each(function() {
                          $(this).find("td:nth-child(5) .timeDisplay").map(function () {
                              if ($(this).val() == '0.00' || !$(this).val()) {
                                disableState =true;
                              }
                          });
                          $(this).find("td:nth-child(1) .dtp_input2").map(function () {
                              if (!$(this).val()) {
                                disableState =true;
                              }
                          });

                        });

                        if (disableState) {
                          $('#err_msg').hide();
                        //  $('#err_msg2').show();
                          $('#re_submit').attr('disabled','disabled');
                        }else {
                          $('#err_msg').hide();
                          ///$('#err_msg2').hide();
                          $('#re_submit').removeAttr('disabled');
                        }
                      }

                      $('#myTable').dataTable({
                        "order": [[ 0, "desc" ]]
                      });
                    });


                })(jQuery);
            </script>
            <style>
              @media screen {
                #printSection {
                    display: none;
                }
              }

              .modal-backdrop{
                z-index: -1 !important
              }

              .dataTables_wrapper .dataTables_paginate{
                width: 25% !important;
              }
              .next {
                  background: none;
              }




              @media print {
                body * {
                  visibility:hidden;
                }

                td {
                  margin-right: 50px !important;
                }

                #printSection, #printSection * {
                  visibility:visible;
                }
                #printSection {
                  position:absolute;
                  left:0;
                  top:0;
                  width:100%;
                  height:100%;
                }
                .no-print, .no-print *
                {
                    display: none !important;
                }


              }

            </style>


            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

            <form class="wpuf-form-add wpuf-form-<?php echo $layout; ?>" action="" method="post">
              <table id="myTable" class="table table-hover"  >
                <thead class="thead-dark">
                  <tr>
                    <th scope="col">Date</th>
                    <th scope="col">Name</th>
                    <th style="display:none" scope="col">Title</th>
                    <th scope="col">Email</th>
                    <th scope="col">Type</th>
                    <th style="display:none" scope="col">Company Name</th>
                    <th scope="col">Approver</th>
                    <th scope="col">Approver Email</th>
                    <th scope="col">Status</th>
                    <th style="display:none" scope="col">Comment</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                    do_shortcode( '[wpuf_time_sheet_list]' )
                  ?>

                </tbody>
              </table>
            </form>
          <?php
        }
        else {
          if ( $form_vars ) {

              ?>
                <form class="wpuf-form-add wpuf-form-<?php echo $layout; ?>" action="" method="post">

                    <ul class="wpuf-form form-label-<?php echo $label_position; ?>">

                        <?php
                        if ( !$post_id ) {
                            do_action( 'wpuf_add_post_form_top', $form_id, $form_settings );
                        } else {
                            do_action( 'wpuf_edit_post_form_top', $form_id, $post_id, $form_settings );
                        }

                        if ( !is_user_logged_in() && $form_settings['guest_post'] == 'true' && $form_settings['guest_details'] == 'true' ) {
                            $this->guest_fields( $form_settings );
                        }

                          do_shortcode( '[wpuf_current_user_data]' );
                          $this->render_items( $form_vars, $post_id, 'post', $form_id, $form_settings );
                        ?>
                        <meta http-equiv='cache-control' content='no-cache'>
                        <meta http-equiv='expires' content='0'>
                        <meta http-equiv='pragma' content='no-cache'>
                        <link rel="stylesheet" href="<?php echo plugins_url( 'assets/css/bootstrap.css', dirname( __FILE__ ) ); ?>">

                        <link rel="stylesheet" href="<?php echo plugins_url( 'assets/datepicker/bootstrap-timepicker.min.css', dirname( __FILE__ ) ); ?>">

                        <script src="<?php echo plugins_url( 'assets/js/timepicki.js', dirname( __FILE__ ) ); ?>"></script>

                        <script src="<?php echo plugins_url( 'assets/datepicker/bootstrap/js/bootstrap.min.js', dirname( __FILE__ ) ); ?>"></script>
                        <script src="<?php echo plugins_url( 'assets/datepicker/bootstrap-datetimepicker.js', dirname( __FILE__ ) ); ?>"></script>
                        <script src="<?php echo plugins_url( 'assets/datepicker/bootstrap-timepicker.min.js', dirname( __FILE__ ) ); ?>"></script>
                        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.js"></script>

                        <style type="text/css">
                          .addDays{
                            font-size: 16px;
                            padding: 0px 15px;
                            border: 1px solid #ccc;
                            -webkit-border-radius: 3px;
                            -moz-border-radius: 3px;
                            border-radius: 3px;
                            background: #d9534f;
                            border-color: #d9534f #d9534f #d9534f;
                            -webkit-box-shadow: 0 1px 0 #d9534f;
                            box-shadow: 0 1px 0 #d9534f;
                            color: #fff;
                            text-decoration: none;
                            margin-bottom: 25px;
                            text-shadow: 0 -1px 1px #d9534f, 1px 0 1px #d9534f, 0 1px 1px #d9534f, -1px 0 1px #d9534f;
                          }
                          .inputText{
                            border-radius: 4px;
                            border: 1px solid #ccc;
                            padding: 4px 2px;
                            text-align: center;
                            width: 80px;
                            height: 30px;
                          }
                          .inputText2{
                            border-radius: 4px;
                            border: 1px solid #ccc;
                            padding: 4px 2px;
                            height: 30px;
                            text-align: center;
                          }
                          ul.wpuf-form li{
                            margin-left: 0;
                            margin-bottom: 0px !important;
                            padding: 5px !important;
                          }
                          #left-area ul, .comment-content ul, .entry-content ul, body.et-pb-preview #main-content .container ul{
                            line-height: 20px !important;
                          }
                          ul.wpuf-form li .wpuf-fields input[type="text"], ul.wpuf-form li .wpuf-fields input[type="password"], ul.wpuf-form li .wpuf-fields input[type="email"], ul.wpuf-form li .wpuf-fields input[type="url"], ul.wpuf-form li .wpuf-fields input[type="number"], ul.wpuf-form li .wpuf-fields textarea {
                            border-radius: 5px;
                          }
                          ul.wpuf-form li .wpuf-label{
                            width: 20% !important;
                          }

                          #cnfrm_1{
                            margin-top: 35px;
                            margin-bottom: 10px;
                          }

                          h2 {
                            font-size: 20px !important;
                            margin-top: 0px !important;
                          }

                          label {
                            font-weight: normal !important;
                          }
                          .entry-content tr td, body.et-pb-preview #main-content .container tr td{
                            padding: 0px 8px;
                          }

                          table#timeSheetTable td {
                              width: 160px !important;
                          }
                          #timeSheetTable{
                            border: none;
                          }
                        </style>
                        <script type="text/javascript">
                            ;(function($) {
                                $(document).ready( function(){
                                  //$('html[manifest=saveappoffline.appcache]').attr('content', '');
                                  var row_count = 0;
                                  $('select option:selected').removeAttr('selected');
                                  $('#your_name_1170').val($('#temp_name').val()).prop("readonly", true);
                                  $('#your_title_1170').val($('#temp_title').val()).prop("readonly", true);
                                  $('#wpuf-your_email').val($('#temp_email').val()).prop("readonly", true);

                                  $('#wpuf-approver_email').blur(function (){

                                    if ($(this).val() != null) {
                                      if (  $(this).val() == $('#wpuf-your_email').val() ) {
                                        alert('Contractor and approver email cannot be identical!');
                                        $('#wpuf-approver_email').val('');
                                      }
                                    }
                                  });

                                  function getTotal() {
                                    var sum = 0;
                                    var sum2 = 0;
                                    // $(".day_sum").each(function() {
                                    //     var value = $(this).val();
                                    //     // add only if the value is number
                                    //     if(!isNaN(value) && value.length != 0) {
                                    //         var full_time = value.split('.');
                                    //         var hr = full_time[0];
                                    //         var min = full_time[1];
                                    //         var fullmin = (parseInt(hr)*60)+parseInt(min);
                                    //         sum += parseInt(fullmin);
                                    //     }
                                    // });
                                    $(".custom_html_1 tr").each(function() {
                                      $(this).find("td:nth-child(5) .day_sum").map(function () {
                                        var value = $(this).val();
                                        // add only if the value is number
                                        if(!isNaN(value) && value.length != 0) {
                                            var full_time = value.split('.');
                                            var hr = full_time[0];
                                            var min = full_time[1];
                                            var fullmin = (parseInt(hr)*60)+parseInt(min);
                                            sum += parseInt(fullmin);
                                            sum2 = parseFloat(sum2)+parseFloat(value);
                                        }
                                      });
                                    });

                                    var full_hr = parseInt(sum/60);
                                    var full_min = sum%60;
                                    var flat_min =0;
                                    if (full_min.toString().length == 1) {
                                      flat_min = '0'+full_min
                                    }
                                    else {
                                      flat_min = full_min;
                                    }


                                    //var day_time = (parseFloat(new_time).toFixed(2))/7.5
                                    //parseFloat(amount).toFixed(1);
                                    if ($('.wpuf_contractor_type_1170').val() == 2) {
                                      //var day_time = (parseFloat(sum).toFixed(2))/7.5
                                      $('.custom_html_1 table tr').each(function() {
                                        $(this).find('#txt_total').val(sum2.toFixed(2));
                                        $(this).find('.txt_total').val(sum2.toFixed(2));
                                      });
                                      // $('#txt_total').val(sum2.toFixed(2));
                                      // $('.txt_total').val(sum2.toFixed(2));
                                    }else {
                                      $('.custom_html_1 table tr').each(function() {
                                        $(this).find('#txt_total').val(full_hr+'.'+flat_min);
                                        $(this).find('.txt_total').val(full_hr+'.'+flat_min);
                                      });
                                      // $('#txt_total').val(full_hr+'.'+flat_min);
                                      // $('.txt_total').val(full_hr+'.'+flat_min);
                                    }
                                    // $('#txt_total').val(full_hr+'.'+flat_min);
                                    // $('.txt_total').val(full_hr+'.'+flat_min);
                                    //return sum;
                                  }

                                  function appendTable(para=1) {
                                    var row_count2 = $('.custom_html_1 #timeSheetTable #timeSheetTableBody').children('tr').length+1;
                                    return(
                                      "<tr>"+
                                        "<td style='border:0px;'>"+
                                          "<div class='form-group'>"+
                                              "<div class='input-group date form_date  ' data-date-format='dd-mm-yyyy' data-link-field='dtp_input2"+row_count2+"' data-link-format='yyyy-mm-dd'>"+
                                                  "<input class='form-control' size='16' type='text' value='' readonly>"+
                                                  "<span class='input-group-addon'><span class='glyphicon glyphicon-calendar'></span></span>"+
                                              "</div>"+
                                              "<input class='dtp_input2' type='hidden' id='dtp_input2"+row_count2+"' value='' />"+
                                          "</div>"+
                                        "</td>"+
                                        "<td style='border:0px;'>"+
                                          "<div class='form-group'>"+
                                              "<div style='width: 100%' class='input-group bootstrap-timepicker timepicker'>"+
                                                  "<input   type='text' class='timepicker1 form-control input-small'>"+
                                                  "<span class='input-group-addon'><i class='glyphicon glyphicon-time'></i></span>"+
                                              "</div>"+
                                              "<input  type='hidden' class='timeDisplay'>"+
                                          "</div>"+
                                        "</td>"+
                                        "<td style='border:0px;'>"+
                                          "<div class='form-group'>"+
                                              "<div  style='width: 100%' class='input-group bootstrap-timepicker timepicker'>"+
                                                  "<input  type='text' class='timepicker2 form-control input-small'>"+
                                                  "<span class='input-group-addon'><i class='glyphicon glyphicon-time'></i></span>"+
                                              "</div>"+
                                              "<input  type='hidden' class='timeDisplay' value='0'>"+
                                          "</div>"+
                                        "</td>"+
                                        "<td style='border:0px;'>"+
                                          "<div class='form-group'>"+
                                              "<div  style='width: 100%' class='input-group bootstrap-timepicker timepicker'>"+
                                                  "<input  type='text' class='timepicker3 form-control input-small'>"+
                                                  "<span class='input-group-addon'><i class='glyphicon glyphicon-time'></i></span>"+
                                              "</div>"+
                                              "<input  type='hidden' class='timeDisplay'>"+
                                          "</div>"+
                                        "</td>"+
                                        "<td style='border:0px;'>"+
                                          "<div class='form-group'>"+
                                              "<div style='display: ruby; white-space: nowrap; width: 100%' >"+
                                                  "<input  type='text' class='day_sum inputText' readonly>"+removeButton(para)+
                                              "</div>"+
                                              "<input type='hidden' class='timeDisplay'>"+
                                          "</div>"+
                                        "</td>"+
                                      "</tr>"
                                    )
                                  }

                                  function footerTable(){

                                    return(
                                      "<tr>"+
                                        "<td style='border:0px;'></td>"+
                                        "<td style='border:0px;'></td>"+
                                        "<td style='border:0px;'></td>"+
                                        "<td style='border:0px;padding-left: 120px;'>Total</td>"+
                                        "<td style='border:0px;'>"+
                                          "<div class='form-group'>"+
                                              "<div style='display: ruby; white-space: nowrap; width: 100%' >"+
                                                  "<input id='txt_total' type='text' class=' inputText' readonly>"+
                                              "</div>"+
                                              "<input  type='hidden' class='txt_total timeDisplay'>"+
                                          "</div>"+
                                        "</td>"+
                                      "</tr>"
                                    );
                                  }

                                  function removeButton(obj) {
                                    if (obj == 1) {
                                      return("<span></span>");
                                    }
                                    else {
                                      return("<span style='margin-left:10px;cursor: pointer;' class='del' ><i class='glyphicon glyphicon-remove-circle'></i></span>");
                                    }
                                  }

                                  function commondatepcker(){

                                    $('.form_date').datetimepicker({
                                      weekStart: 1,
                                      todayBtn:  1,
                                      autoclose: 1,
                                      todayHighlight: 1,
                                      startView: 2,
                                      minView: 2,
                                      forceParse: 0,
                                    });
                                    $('.datetimepicker').click(function() {
                                      validateTable();
                                    });
                                    $('.timepicker1').timepicker({
                                      defaultTime: false
                                    }).on('changeTime.timepicker', function(e) {
                                        var newTime = $(this).val();
                                        $(this).closest('tr').find('td:eq(3)').each(function() {
                                          $(this).find('.timepicker3').timepicker('setTime', newTime);
                                        });
                                        validateTable();
                                      });

                                    $('.timepicker2').timepicker({
                                      showMeridian:false,
                                      defaultTime: '0.00'
                                    }).on('changeTime.timepicker', function(e) {
                                      var newTime = 0;
                                      $(this).closest('tr').find('td:eq(1)').each(function() {
                                        newTime = $(this).find('.timepicker1').val();
                                        //$('.timepicker3').timepicker('setTime', $('.timepicker1').val());
                                        //return false;
                                      });
                                      $(this).closest('tr').find('td:eq(3)').each(function() {
                                        $(this).find('.timepicker3').timepicker('setTime', newTime);
                                        //$('.timepicker3').timepicker('setTime', $('.timepicker1').val());
                                        //return false;
                                      });
                                      validateTable();

                                    });

                                    $('.timepicker3').timepicker({
                                      defaultTime: false
                                    }).on('changeTime.timepicker', function(e) {
                                        var h= e.time.hours;
                                        var m= e.time.minutes;
                                        var mer= e.time.meridian;
                                        var time1 = 0;
                                        var Time = 0;
                                        $(this).closest('tr').find('td:eq(1)').each(function() {
                                          time1 = $(this).find('.timepicker1').val();
                                        });

                                        var timeStringArray = time1.split(':');
                                        if (timeStringArray.length > 1) {
                                           Time =  timeStringArray[1].split(' ');
                                        }


                                        if (Time[1] == 'AM') {
                                          if (mer == 'PM') {
                                            if(h != 12){
                                              h= h+12;
                                            }
                                          }
                                          else if(h == 12){
                                            h= h-12;
                                          }else {
                                            h= h
                                          }
                                        }else {
                                          if(h == 12){
                                            h= h-12;
                                          }
                                        }

                                        //convert hours into minutes
                                        m+=h*60;
                                        //10:15 = 10h*60m + 15m = 615 min

                                        $(this).closest('tr').find('td:eq(1)').each(function() {
                                           var status = $(this).find('.timeDisplay').val();
                                           var timeStringArray = status.split(':');
                                           if (timeStringArray.length > 1) {


                                             var Time2 =  timeStringArray[1].split(' ');
                                             var timeString = parseInt(timeStringArray[0]) * 60 ;

                                             timeString += parseInt(Time2[0]);

                                             if (Time2[1] == 'AM' && timeStringArray[0] == 12) {
                                               timeString= (timeString % 60);
                                             }

                                             if (mer==Time2[1] && Time2[1] == 'PM' && timeStringArray[0] == 12) {
                                               timeString= (timeString % 60);
                                             }



                                             if(mer==Time2[1] && m<timeString){
                                               $(this).closest('tr').find('td:eq(3)').each(function() {
                                                 $(this).find('.timepicker3').timepicker('setTime', status);
                                               })
                                             }else {
                                               var minute_time = (m - timeString) % 60;
                                               var hour_time = Math.floor((m - timeString) / 60);

                                               $(this).closest('tr').find('td:eq(4)').each(function() {
                                                 var new_hours2 = 0;
                                                 var new_min2 = 0;
                                                 $(this).closest('tr').find('td:eq(2)').each(function() {

                                                   var status2 = $(this).find('.timepicker2').val();
                                                   var timeStringArray2 = status2.split(':');
                                                   new_hours2 =  parseInt(timeStringArray2[0]) ;
                                                   new_min2 =  parseInt(timeStringArray2[1]) ;

                                                 });

                                                 if (new_hours2 > hour_time) {
                                                   $(this).find('.day_sum').val('0.00');
                                                   $(this).find('.timeDisplay').val('0.00');
                                                 }
                                                 else if (mer == 'AM' && Time[1] == 'PM') {
                                                   $(this).find('.day_sum').val('0.00');
                                                   $(this).find('.timeDisplay').val('0.00');
                                                 }
                                                 else {
                                                   var h_time = hour_time - new_hours2;
                                                   var m_time = 0;
                                                   if (new_min2 > minute_time) {
                                                     if (h_time > 0) {
                                                       m_time = (parseInt(minute_time) + 60) - new_min2;
                                                       h_time =h_time - 1;
                                                       var m_time_1 = 0;
                                                       if (m_time.toString().length == 1) {
                                                         m_time_1 = '0'+m_time
                                                       }else {
                                                         m_time_1 = m_time;
                                                       }

                                                       var new_time = h_time+'.'+m_time_1;
                                                       //$('.wpuf_contractor_type_1170').val();
                                                       var newTime2 = (parseInt(h_time)*60) + parseInt(m_time_1);
                                                       var day_time = (parseFloat(newTime2).toFixed(2))/450;
                                                       //parseFloat(amount).toFixed(1);
                                                       if ($('.wpuf_contractor_type_1170').val() == 2) {
                                                         $(this).find('.day_sum').val(day_time.toFixed(2));
                                                         $(this).find('.timeDisplay').val(day_time.toFixed(2));
                                                       }else {
                                                         $(this).find('.day_sum').val(new_time);
                                                         $(this).find('.timeDisplay').val(new_time);
                                                       }

                                                     }
                                                     else {
                                                       $(this).find('.day_sum').val('0.00');
                                                       $(this).find('.timeDisplay').val('0.00');
                                                     }

                                                   }
                                                   else {
                                                     m_time = parseInt(minute_time)  - new_min2;
                                                     var m_time_1 = 0;

                                                     if (m_time.toString().length == 1) {
                                                       m_time_1 = '0'+m_time
                                                     }else {
                                                       m_time_1 = m_time;
                                                     }
                                                     var new_time = h_time+'.'+m_time_1;
                                                     var newTime2 = (parseInt(h_time)*60) + parseInt(m_time_1);
                                                     var day_time = (parseFloat(newTime2).toFixed(2))/450;
                                                     //parseFloat(amount).toFixed(1);
                                                     if ($('.wpuf_contractor_type_1170').val() == 2) {
                                                       $(this).find('.day_sum').val(day_time.toFixed(2));
                                                       $(this).find('.timeDisplay').val(day_time.toFixed(2));
                                                     }else {

                                                       $(this).find('.day_sum').val(new_time);
                                                       $(this).find('.timeDisplay').val(new_time);
                                                     }
                                                   }
                                                 }
                                               });
                                             }
                                           }
                                        });

                                        getTotal();
                                        validateTable();

                                      });

                                    $('.timepicker1').change(function() {
                                      $(this).parent().siblings(".timeDisplay").val($(this).val()) ;
                                      validateTable();
                                    });

                                    $('.timepicker2').change(function() {
                                      $(this).parent().siblings(".timeDisplay").val($(this).val()) ;
                                      validateTable();
                                    });

                                    $('.timepicker3').change(function() {
                                      $(this).parent().siblings(".timeDisplay").val($(this).val()) ;
                                      validateTable();
                                    });

                                    validateTable();
                                  }

                                  function renderTable(data){
                                    $('.custom_html_1').append("<p id='err_msg' class='no-print' style='color: red;visibility:idden;padding-bottom: 0px;margin-bottom: 0px;'>Complete the timesheet with valid time entries. </p>");
                                    //$('.custom_html_1').append("<p id='err_msg2'class='no-print' style='color: red;visibility:idden;'>Hours cannot be zero </p>")
                                    $('.custom_html_1').append(
                                      "<table class='myTable_content'  id='timeSheetTable'>"+
                                        "<thead>"+
                                          "<tr>"+
                                            "<th>Date</th>"+
                                            "<th>Start Time</th>"+
                                            "<th>Break Time</th>"+
                                            "<th>End Time</th>"+
                                            "<th>"+data+"</th>"+
                                            "<th></th>"+
                                          "</tr>"+
                                        "</thead>"+
                                        "<tbody id='timeSheetTableBody'>"+appendTable()+"</tbody>"+
                                        "<tbody id='timeSheetTableBody2'>"+footerTable()+"</tbody>"+
                                      "</table>"
                                    );

                                    $('.custom_html_1').append("<button id='BTN_ADD' type='button' class='addDays'>+ Day</button>");


                                    $('#BTN_ADD').click(function() {
                                      var row_count2 = $('.custom_html_1 #timeSheetTable #timeSheetTableBody').children('tr').length;
                                      console.log('row_count2 ',row_count2);
                                      if (row_count2 < 7) {
                                        $('#timeSheetTableBody').append(appendTable('remove'));
                                        commondatepcker();

                                        $(".del").bind('click', function() {

                                           $(this).closest('tr').remove();
                                           var count = $('#mdTable #timeSheetTableBody').children('tr').length;
                                           row_count = count-1;

                                           getTotal();

                                       });
                                      }
                                        validateTable();


                                    });

                                    var specifiedElement = document.getElementById('timeSheetTable');
                                    document.addEventListener('click', function(event) {
                                        var isClickInside = specifiedElement.contains(event.target);
                                        if (isClickInside) {
                                          //console.log('You clicked inside')
                                        }
                                        else {
                                          validateTable();
                                        }
                                    });



                                    commondatepcker();
                                  }

                                  function validateTable(){
                                    var disableState =false;
                                    //var disableState2 =false;
                                    // $("#timeSheetTable").find("input:hidden").map(function () {
                                    //
                                    //     if (!$(this).val()) {
                                    //       disableState =true;
                                    //     }
                                    // });


                                    $(".custom_html_1 tr").each(function() {
                                      $(this).find("td:nth-child(5) .day_sum").map(function () {
                                        var value = $(this).val();
                                        // add only if the value is number
                                        if (value == '0.00' || value == '') {
                                          disableState =true;
                                        }
                                      });

                                      $(this).find("td:nth-child(1) .dtp_input2").map(function () {
                                          if (!$(this).val()) {
                                            disableState =true;
                                          }
                                      });
                                    });



                                    if (disableState) {
                                      $('#err_msg').show();
                                      //$('#err_msg2').show();
                                      $('#submit_form').attr('disabled','disabled');
                                    }else {
                                      $('#err_msg').hide();
                                      //$('#err_msg2').hide();
                                      $('#submit_form').removeAttr('disabled');
                                    }
                                  }

                                  $( "select" ).change(function () {
                                    if ($( this ).val() == '1') {
                                      row_count = 0;
                                      $('#lbl_hr').show();
                                      $('#lbl_days').hide();
                                      $('.custom_html_1').html("");
                                      renderTable('Hours');
                                      $('#submit_form').click(function (){
                                        $('#timeDetails').val($(".custom_html_1").html());

                                        var jHtmlObject = jQuery($("#timeSheetTable").html());
                                        var editor = $("<p>").append(jHtmlObject);
                                        editor.find(".input-group").remove();
                                        editor.find(".inputText").remove();
                                        editor.find(".form-group div").remove();

                                        var newHtml = editor.html();
                                        $('#timeDetails3').val(newHtml);
                                        $('#timeDetails2').val(moment().format("YYYY-MM-DD"));
                                      });
                                    }
                                    else if ($( this ).val() == '2') {
                                      row_count = 0;
                                      $('#lbl_days').show();
                                      $('#lbl_hr').hide();
                                      $('.custom_html_1').html("");
                                      renderTable('Days');
                                      $('#submit_form').click(function (){
                                        $('#timeDetails').val($(".custom_html_1").html());
                                        var jHtmlObject = jQuery($("#timeSheetTable").html());
                                        var editor = $("<p>").append(jHtmlObject);
                                        editor.find(".input-group").remove();
                                        editor.find(".inputText").remove();
                                        editor.find(".form-group div").remove();
                                        
                                        var newHtml = editor.html();
                                        $('#timeDetails3').val(newHtml);
                                        $('#timeDetails2').val(moment().format("YYYY-MM-DD"));
                                      });
                                    }else {
                                      $('#lbl_hr').hide();
                                      $('#lbl_days').hide();
                                      $('.custom_html_1').html("");
                                    }

                                  });

                                  $('#clear').click(function (){
                                    $('.custom_html_1').html("");
                                    //company_name_1170
                                    //approver_name_1170
                                    //wpuf-approver_email
                                    $('#company_name_1170').val('');
                                    $('#approver_name_1170').val('');
                                    $('#wpuf-approver_email').val('');
                                    $('#lbl_hr').hide();
                                    $('#lbl_days').hide();


                                    $('select option:selected').removeAttr('selected');
                                    $('#your_name_1170').val($('#temp_name').val()).prop("readonly", true);
                                    $('#your_title_1170').val($('#temp_title').val()).prop("readonly", true);
                                    $('#wpuf-your_email').val($('#temp_email').val()).prop("readonly", true);
                                  });

                                    // $('#submit_form').click(function (){
                                    //   $('#timeDetails').val($(".custom_html_1").html());
                                    // });

                                    //$('.timeDisplay').val($('.timepicker1').val());

                                    //timeDetails/submit_form
                                    //$('li.tags input[name=tags]').suggest( wpuf_frontend.ajaxurl + '?action=wpuf-ajax-tag-search&tax=post_tag', { delay: 500, minchars: 2, multiple: true, multipleSep: ', ' } );
                                });
                            })(jQuery);
                        </script>

                        <?php
                        $this->submit_button( $form_id, $form_settings, $post_id );
                        if ( !$post_id ) {
                            do_action( 'wpuf_add_post_form_bottom', $form_id, $form_settings );
                        } else {
                            do_action( 'wpuf_edit_post_form_bottom', $form_id, $post_id, $form_settings );
                        }
                        ?>


                    </ul>
                    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" />
                </form>


              <?php
          } //endif
        }


    }

    function render_item_before( $form_field, $post_id ) {
        $label_exclude = array('section_break', 'html', 'action_hook', 'toc', 'shortcode');
        $el_name       = !empty( $form_field['name'] ) ? $form_field['name'] : '';
        $class_name    = !empty( $form_field['css'] ) ? ' ' . $form_field['css'] : '';
        $field_size    = !empty( $form_field['width'] ) ? ' field-size-' . $form_field['width'] : '';

        printf( '<li class="wpuf-el %s%s%s" data-label="%s">', $el_name, $class_name, $field_size, $form_field['label'] );

        if ( isset( $form_field['input_type'] ) && !in_array( $form_field['input_type'], $label_exclude ) ) {
            $this->label( $form_field, $post_id );
        }
    }

    function render_item_after( $form_field ) {
        echo '</li>';
    }

    function conditional_logic( $form_field, $form_id ) {

        $cond_inputs = $form_field['wpuf_cond'];
        $cond_inputs['condition_status'] = isset( $cond_inputs['condition_status'] ) ? $cond_inputs['condition_status'] : '';

        if ( $cond_inputs['condition_status'] == 'yes') {
            $cond_inputs['type']    = $form_field['input_type'];
            $cond_inputs['name']    = $form_field['name'];
            $cond_inputs['form_id'] = $form_id;
            $condition              = json_encode( $cond_inputs );

        } else {
            $condition = '';
        }

        //taxnomy name create unique
        if ( $form_field['input_type'] == 'taxonomy' ) {
            $cond_inputs['name'] = $form_field['name'] . '_' . $form_field['type'] .'_'. $form_field['id'];
            $condition           = json_encode( $cond_inputs );
        }

        //for section break
        if ( $form_field['input_type'] == 'section_break' ) {
            $cond_inputs['name'] = $form_field['name'] .'_'. $form_field['id'];
            $condition           = json_encode( $cond_inputs );
        }

        ?>
        <script type="text/javascript">
            wpuf_conditional_items.push(<?php echo $condition; ?>);
        </script>
        <?php
    }

    /**
     * Render form items
     *
     * @param array $form_vars
     * @param int|null $post_id
     * @param string $type type of the form. post or user
     */
    function render_items( $form_vars, $post_id, $type = 'post', $form_id, $form_settings, $cond_inputs = array() ) {

        $edit_ignore = array( 'really_simple_captcha' );
        $hidden_fields = array();
        ?>
        <script type="text/javascript">
            if ( typeof wpuf_conditional_items === 'undefined' ) {
                wpuf_conditional_items = [];
            }

            if ( typeof wpuf_plupload_items === 'undefined' ) {
                wpuf_plupload_items = [];
            }

            if ( typeof wpuf_map_items === 'undefined' ) {
                wpuf_map_items = [];
            }
        </script>
        <?php

        //through var, we will know if multiform step started already
        //$multiform_start = 0;

        //if multistep form is enabled
        if ( isset( $form_settings['enable_multistep'] ) && $form_settings['enable_multistep'] == 'yes' ) {
            ?>
            <input type="hidden" name="wpuf_multistep_type" value="<?php echo $form_settings['multistep_progressbar_type'] ?>"/>
            <?php
            if ( $form_settings['multistep_progressbar_type'] == 'step_by_step' ){
                ?>
                <!--wpuf-multistep-progressbar-> wpuf_ms_pb-->
                <div class="wpuf-multistep-progressbar">

                </div>
            <?php
            } else {
                ?>
                <div class="wpuf-multistep-progressbar">

                </div>
            <?php

            }

        }

        foreach ($form_vars as $key => $form_field) {

            // check field visibility options
            if ( array_key_exists( 'wpuf_visibility', $form_field ) ) {

                $visibility_selected = $form_field['wpuf_visibility']['selected'];
                $visibility_choices  = $form_field['wpuf_visibility']['choices'];
                $show_field = false;

                if ( $visibility_selected == 'everyone' ) {
                    $show_field = true;
                }

                if ( $visibility_selected == 'hidden' ) {
                    $form_field['css'] .= ' wpuf_hidden_field';
                    $show_field = true;
                }

                if ( $visibility_selected == 'logged_in' && is_user_logged_in() ) {

                    if ( empty($visibility_choices) ) {
                        $show_field = true;
                    }else{
                        foreach ( $visibility_choices as $key => $choice ) {
                            if( current_user_can( $choice ) ) {
                                $show_field = true;
                                break;
                            }
                            continue;
                        }
                    }

                }

                if ( $visibility_selected == 'subscribed_users' && is_user_logged_in() ) {

                    $user_pack  = WPUF_Subscription::init()->get_user_pack(get_current_user_id());

                    if ( empty( $visibility_choices ) && !empty( $user_pack ) ) {
                        $show_field = true;
                    }elseif( !empty( $user_pack ) && !empty( $visibility_choices ) ) {

                        foreach ( $visibility_choices as $pack => $id ) {
                            if ( $user_pack['pack_id'] == $id ) {
                                $show_field = true;
                                break;
                            }
                            continue;
                        }

                    }

                }

                if ( !$show_field ) {
                    break;
                }
            }

            // don't show captcha in edit page
            if ( $post_id && in_array( $form_field['input_type'], $edit_ignore ) ) {
                continue;
            }

            // igonre the hidden fields
            if ( $form_field['input_type'] == 'hidden' ) {
                $hidden_fields[] = $form_field;
                continue;
            }

            if ( $form_field['input_type'] != 'step_start' && $form_field['input_type'] != 'step_end' ) {
                $this->render_item_before( $form_field, $post_id );
            }

            $this->field_count++;

            switch ($form_field['input_type']) {
                case 'text':
                    $this->text( $form_field, $post_id, $type, $form_id );
                    $this->conditional_logic( $form_field, $form_id );
                    break;

                case 'textarea':
                    $this->textarea( $form_field, $post_id, $type, $form_id );
                    $this->conditional_logic( $form_field, $form_id );
                    break;

                case 'select':
                    $this->select( $form_field, false, $post_id, $type, $form_id );
                    $this->conditional_logic( $form_field, $form_id );
                    break;

                case 'multiselect':
                    $this->select( $form_field, true, $post_id, $type, $form_id );
                    $this->conditional_logic( $form_field, $form_id );
                    break;

                case 'radio':
                    $this->radio( $form_field, $post_id, $type, $form_id );
                    $this->conditional_logic( $form_field, $form_id );
                    break;

                case 'checkbox':
                    $this->checkbox( $form_field, $post_id, $type, $form_id );
                    $this->conditional_logic( $form_field, $form_id );
                    break;

                case 'url':
                    $this->url( $form_field, $post_id, $type, $form_id );
                    $this->conditional_logic( $form_field, $form_id );
                    break;

                case 'email':
                    $this->email( $form_field, $post_id, $type, $form_id );
                    $this->conditional_logic( $form_field, $form_id );
                    break;

                case 'password':
                    $this->password( $form_field, $post_id, $type, $form_id );
                    $this->conditional_logic( $form_field, $form_id );
                    break;

                case 'taxonomy':

                    $this->taxonomy( $form_field, $post_id, $form_id );
                    $this->conditional_logic( $form_field, $form_id );
                    break;

                case 'section_break':
                    $form_field['name'] = 'section_break';
                    $this->section_break( $form_field, $post_id, $form_id );
                    $this->conditional_logic( $form_field, $form_id );
                    break;

                case 'html':
                    $form_field['name'] = 'custom_html_'.str_replace( ' ','_', $form_field['label'] );

                    $this->html( $form_field, $form_id );
                    $this->conditional_logic( $form_field, $form_id );
                    break;

                case 'image_upload':
                    $this->image_upload( $form_field, $post_id, $type, $form_id );
                    $this->conditional_logic( $form_field, $form_id );
                    break;

                case 'recaptcha':
                    $this->recaptcha( $form_field, $post_id, $form_id );
                    $this->conditional_logic( $form_field, $form_id );
                    break;

                default:

                    // fallback for a dynamic method of this class if exists
                    $dynamic_method = 'field_' . $form_field['input_type'];

                    if ( method_exists( $this, $dynamic_method ) ) {
                        $this->{$dynamic_method}( $form_field, $post_id, $type, $form_id );
                    }

                    do_action( 'wpuf_render_form_' . $form_field['input_type'], $form_field, $form_id, $post_id, $form_settings );
                    do_action( 'wpuf_render_pro_' . $form_field['input_type'], $form_field, $post_id, $type, $form_id, $form_settings, 'WPUF_Render_Form', $this, $this->multiform_start, isset( $form_settings['enable_multistep'] )?$form_settings['enable_multistep']:'' );
                    break;
            }


            $this->render_item_after( $form_field );
        } //end foreach

        if ( $hidden_fields ) {
            foreach($hidden_fields as $field) {
                printf( '<input type="hidden" name="%s" value="%s">', esc_attr( $field['name'] ), esc_attr( $field['meta_value'] ) );
                echo "\r\n";
            }
        }
    }

    function submit_button( $form_id, $form_settings, $post_id ) {
        ?>
        <li class="wpuf-submit">


            <?php wp_nonce_field( 'wpuf_form_add' ); ?>

            <input type="hidden" name="action" value="wpuf_submit_post">

            <?php
            if ( $post_id ) {
                $cur_post = get_post( $post_id );
                ?>
                <input id='timeDetails' type="hidden" name="time_sheet_details"  >
                <input id='timeDetails3' type="text" style="display:none" name="time_sheet_details_table"  >
                <input id='timeDetails2' type="hidden" name="time_sheet_date"  >
                <input type="hidden" name="post_id" value="<?php echo $post_id; ?>">
                <input type="hidden" name="post_date" value="<?php echo esc_attr( $cur_post->post_date ); ?>">
                <input type="hidden" name="comment_status" value="<?php echo esc_attr( $cur_post->comment_status ); ?>">
                <input type="hidden" name="post_author" value="<?php echo esc_attr( $cur_post->post_author ); ?>">
                <input id='clear' type="button" class="btn btn-default" name="clear" value="Clear" />
                <input type="submit" id='submit_form' class="wpuf-submit-button" name="submit" value="<?php echo $form_settings['update_text']; ?>" />
            <?php } else { ?>
                <input id='clear' type="button" class="btn btn-default" name="clear" value="Clear" />
                <input type="submit" id='submit_form' class="wpuf-submit-button" name="submit" value="<?php echo $form_settings['submit_text']; ?>" />
                <input type="hidden" name="wpuf_form_status" value="new">
                <input id='timeDetails' type="hidden" name="time_sheet_details"  >
                <input id='timeDetails3' type="text" style="display:none" name="time_sheet_details_table"  >
                <input id='timeDetails2' type="hidden" name="time_sheet_date"  >
            <?php } ?>

            <?php if ( isset( $form_settings['draft_post'] ) && $form_settings['draft_post'] == 'true' ) { ?>
                <a href="#" class="btn" id="wpuf-post-draft"><?php _e( 'Save Draft', 'wpuf' ); ?></a>
            <?php } ?>
        </li>
    <?php
    }

    /**
     * Form preview handler
     *
     * @return void
     */
    function preview_form() {
        $form_id = isset( $_GET['form_id'] ) ? intval( $_GET['form_id'] ) : 0;


        if ( $form_id ) {
            ?>

            <!doctype html>
            <html lang="en">
                <head>
                    <meta charset="UTF-8">
                    <title>Form Preview</title>
                    <link rel="stylesheet" href="<?php echo plugins_url( 'assets/css/frontend-forms.css', dirname( __FILE__ ) ); ?>">

                    <style type="text/css">
                        body {
                            margin: 0;
                            padding: 0;
                            background: #eee;
                        }

                        .container {
                            width: 700px;
                            margin: 0 auto;
                            margin-top: 20px;
                            padding: 20px;
                            background: #fff;
                            border: 1px solid #DFDFDF;
                            -webkit-box-shadow: 1px 1px 2px rgba(0,0,0,0.1);
                            box-shadow: 1px 1px 2px rgba(0,0,0,0.1);
                        }
                    </style>

                    <script type="text/javascript" src="<?php echo includes_url( 'js/jquery/jquery.js' ); ?>"></script>
                </head>
                <body>
                    <div class="container">
                        <?php $this->render_form( $form_id, null ); ?>
                    </div>
                </body>
            </html>

            <?php
        } else {
            wp_die( 'Error generating the form preview' );
        }

        exit;
    }


    /**
     * Prints required field asterisk
     *
     * @param array $attr
     * @return string
     */
    function required_mark( $attr ) {
        if ( isset( $attr['required'] ) && $attr['required'] == 'yes' ) {
            return ' <span class="required">*</span>';
        }
    }

    /**
     * Prints HTML5 required attribute
     *
     * @param array $attr
     * @return string
     */
    function required_html5( $attr ) {
        if ( $attr['required'] == 'yes' ) {
            // echo ' required="required"';
        }
    }

    /**
     * Print required class name
     *
     * @param array $attr
     * @return string
     */
    function required_class( $attr ) {
        return;
        if ( $attr['required'] == 'yes' ) {
            echo ' required';
        }
    }

    /**
     * Prints form input label
     *
     * @param array $attr
     */
    function label( $attr, $post_id = 0 ) {
        if ( $post_id && $attr['input_type'] == 'password') {
            $attr['required'] = 'no';
        }
        if ( isset( $attr['input_type'] ) && $attr['input_type'] == 'recaptcha' && $attr['recaptcha_type'] == 'invisible_recaptcha') {
            return;
        }

        ?>
        <div class="wpuf-label">
            <label for="<?php echo isset( $attr['name'] ) ? $attr['name'] : 'cls'; ?>"><?php echo $attr['label'] . $this->required_mark( $attr ); ?></label>
        </div>
        <?php
    }

    /**
     * Prints help text for a field
     *
     * @param array $attr
     */
    function help_text( $attr ) {
        if ( empty( $attr['help'] ) ) {
            return;
        }
        ?>
        <span class="wpuf-help"><?php echo stripslashes( $attr['help'] ); ?></span>
        <?php
    }

    /**
     * Check if its a meta field
     *
     * @param array $attr
     * @return boolean
     */
    function is_meta( $attr ) {
        if ( isset( $attr['is_meta'] ) && $attr['is_meta'] == 'yes' ) {
            return true;
        }

        return false;
    }

    /**
     * Get a meta value
     *
     * @param int $object_id user_ID or post_ID
     * @param string $meta_key
     * @param string $type post or user
     * @param bool $single
     * @return string
     */
    function get_meta( $object_id, $meta_key, $type = 'post', $single = true ) {
        if ( !$object_id ) {
            return '';
        }

        if ( $type == 'post' ) {
            return get_post_meta( $object_id, $meta_key, $single );
        }

        return get_user_meta( $object_id, $meta_key, $single );
    }

    function get_user_data( $user_id, $field ) {
        return get_user_by( 'id', $user_id )->$field;
    }

    /**
     * Prints a text field
     *
     * @param array $attr
     * @param int|null $post_id
     */
    function text( $attr, $post_id, $type = 'post', $form_id = null ) {
        // checking for user profile username
        $username = false;
        $taxonomy = false;

        if ( $post_id ) {

            if ( $this->is_meta( $attr ) ) {
                $value = $this->get_meta( $post_id, $attr['name'], $type );
            } else {

                // applicable for post tags
                if ( $type == 'post' && $attr['name'] == 'tags' ) {
                    $post_tags = wp_get_post_tags( $post_id );
                    $tagsarray = array();
                    foreach ($post_tags as $tag) {
                        $tagsarray[] = $tag->name;
                    }

                    $value = implode( ', ', $tagsarray );
                    $taxonomy = true;
                } elseif ( $type == 'post' ) {
                    $value = get_post_field( $attr['name'], $post_id );
                } elseif ( $type == 'user' ) {
                    $name = $attr['name'];
                    $value = get_user_by( 'id', $post_id )->$name;
                    if ( $attr['name'] == 'user_login' ) {
                        $username = true;
                    }
                }
            }
        } else {
            $value = $attr['default'];

            if ( $type == 'post' && $attr['name'] == 'tags' ) {
                $taxonomy = true;
            }
        }

        ?>

        <div class="wpuf-fields">
            <input class="textfield<?php echo $this->required_class( $attr );  echo ' wpuf_'.$attr['name'].'_'.$form_id; ?>" id="<?php echo $attr['name'].'_'.$form_id; ?>" type="text" data-required="<?php echo $attr['required'] ?>" data-type="text"<?php $this->required_html5( $attr ); ?> name="<?php echo esc_attr( $attr['name'] ); ?>" placeholder="<?php echo esc_attr( $attr['placeholder'] ); ?>" value="<?php echo esc_attr( $value ) ?>" size="<?php echo esc_attr( $attr['size'] ) ?>" <?php echo $username ? 'disabled' : ''; ?> />
            <span class="wpuf-wordlimit-message wpuf-help"></span>
            <?php $this->help_text( $attr ); ?>

            <?php if ( $taxonomy ) { ?>
            <script type="text/javascript">
                ;(function($) {
                    $(document).ready( function(){
                        $('li.tags input[name=tags]').suggest( wpuf_frontend.ajaxurl + '?action=wpuf-ajax-tag-search&tax=post_tag', { delay: 500, minchars: 2, multiple: true, multipleSep: ', ' } );
                    });
                })(jQuery);
            </script>
            <?php } ?>
        </div>

        <?php
        if ( isset( $attr['word_restriction'] ) && $attr['word_restriction'] ) {
            $this->check_word_restriction_func( $attr['word_restriction'], 'no', $attr['name'] . '_' . $form_id );
        }
    }


    /**
     * Function to check word restriction
     *
     * @param $word_nums number of words allowed
     */
    function check_word_restriction_func($word_nums, $rich_text, $field_name) {
        // bail out if it is dashboard
        if ( is_admin() ) {
            return;
        }
        ?>
        <script type="text/javascript">
            ;(function($) {
                $(document).ready( function(){
                    WP_User_Frontend.editorLimit.bind(<?php printf( '%d, "%s", "%s"', $word_nums, $field_name, $rich_text ); ?>);
                });
            })(jQuery);
        </script>
        <?php

    }

    /**
     * Prints a textarea field
     * @param array $attr
     * @param int|null $post_id
     */
    function textarea( $attr, $post_id, $type, $form_id ) {
        $req_class = ( $attr['required'] == 'yes' ) ? 'required' : 'rich-editor';
        if ( $post_id ) {
            if ( $this->is_meta( $attr ) ) {
                $value = $this->get_meta( $post_id, $attr['name'], $type, true );
            } else {

                if ( $type == 'post' ) {
                    $value = get_post_field( $attr['name'], $post_id );
                } else {
                    $value = $this->get_user_data( $post_id, $attr['name'] );
                }
            }
        } else {
            $value = $attr['default'];
        }
        ?>

        <?php if ( in_array( $attr['rich'], array( 'yes', 'teeny' ) ) ) { ?>
            <div class="wpuf-fields wpuf-rich-validation <?php printf( 'wpuf_%s_%s', $attr['name'], $form_id ); ?>" data-type="rich" data-required="<?php echo esc_attr( $attr['required'] ); ?>" data-id="<?php echo esc_attr( $attr['name'] ) . '_' . $form_id; ?>" data-name="<?php echo esc_attr( $attr['name'] ); ?>">
        <?php } else { ?>
            <div class="wpuf-fields">
        <?php } ?>

            <?php if ( isset( $attr['insert_image'] ) && $attr['insert_image'] == 'yes' ) { ?>
                <div id="wpuf-insert-image-container">
                    <a class="wpuf-button wpuf-insert-image" id="wpuf-insert-image_<?php echo $form_id; ?>" href="#" data-form_id="<?php echo $form_id; ?>">
                        <span class="wpuf-media-icon"></span>
                        <?php _e( 'Insert Photo', 'wpuf' ); ?>
                    </a>
                </div>

                <script type="text/javascript">
                    ;(function($) {
                        $(document).ready( function(){
                            WP_User_Frontend.insertImage('wpuf-insert-image_<?php echo $form_id; ?>', '<?php echo $form_id; ?>');
                        });
                    })(jQuery);
                </script>
            <?php } ?>

            <?php
            $form_settings = wpuf_get_form_settings( $form_id );
            $layout        = isset( $form_settings['form_layout'] ) ? $form_settings['form_layout'] : 'layout1';
            $textarea_id   = $attr['name'] ? $attr['name'] . '_' . $form_id : 'textarea_' . $this->field_count;

            if ( $attr['rich'] == 'yes' ) {
                $editor_settings = array(
                    'textarea_rows' => $attr['rows'],
                    'quicktags'     => false,
                    'media_buttons' => false,
                    'editor_class'  => $req_class,
                    'textarea_name' => $attr['name'],
                    'tinymce'       => array(
                        'content_css'   => WPUF_ASSET_URI . '/css/frontend-form/' . $layout . '.css'
                    )
                );

                $editor_settings = apply_filters( 'wpuf_textarea_editor_args' , $editor_settings );
                wp_editor( $value, $textarea_id, $editor_settings );

            } elseif( $attr['rich'] == 'teeny' ) {

                $editor_settings = array(
                    'textarea_rows' => $attr['rows'],
                    'quicktags'     => false,
                    'media_buttons' => false,
                    'teeny'         => true,
                    'editor_class'  => $req_class,
                    'textarea_name' => $attr['name'],
                    'tinymce'       => array(
                        'content_css'   => WPUF_ASSET_URI . '/css/frontend-form/' . $layout . '.css'
                    )
                );

                $editor_settings = apply_filters( 'wpuf_textarea_editor_args' , $editor_settings );
                wp_editor( $value, $textarea_id, $editor_settings );

            } else {
                ?>
                <textarea class="textareafield<?php echo $this->required_class( $attr ); ?> <?php echo ' wpuf_'.$attr['name'].'_'.$form_id; ?>" id="<?php echo $attr['name'] . '_' . $form_id; ?>" name="<?php echo $attr['name']; ?>" data-required="<?php echo $attr['required'] ?>" data-type="textarea"<?php $this->required_html5( $attr ); ?> placeholder="<?php echo esc_attr( $attr['placeholder'] ); ?>" rows="<?php echo $attr['rows']; ?>" cols="<?php echo $attr['cols']; ?>"><?php echo esc_textarea( $value ) ?></textarea>
                <span class="wpuf-wordlimit-message wpuf-help"></span>
            <?php } ?>
            <?php $this->help_text( $attr ); ?>
        </div>
        <?php

        if ( isset( $attr['word_restriction'] ) && $attr['word_restriction'] ) {
            $this->check_word_restriction_func( $attr['word_restriction'], $attr['rich'], $attr['name'] . '_' . $form_id );
        }
    }


    /**
     * Prints a select or multiselect field
     *
     * @param array $attr
     * @param bool $multiselect
     * @param int|null $post_id
     */
    function select( $attr, $multiselect = false, $post_id, $type, $form_id = null ) {
        if ( $post_id ) {
            $selected = $this->get_meta( $post_id, $attr['name'], $type );
            $selected = $multiselect ? explode( self::$separator, $selected ) : $selected;
        } else {
            $selected = isset( $attr['selected'] ) ? $attr['selected'] : '';
            $selected = $multiselect ? ( is_array( $selected ) ? $selected : array() ) : $selected;
        }

        $name      = $multiselect ? $attr['name'] . '[]' : $attr['name'];
        $multi     = $multiselect ? ' multiple="multiple"' : '';
        $data_type = $multiselect ? 'multiselect' : 'select';
        $css       = $multiselect ? ' class="multiselect  wpuf_'. $attr['name'] .'_'. $form_id.'"' : '';
        ?>

        <div class="wpuf-fields">
            <select <?php echo $css; ?> class="<?php echo 'wpuf_'. $attr['name'] .'_'. $form_id; ?>" name="<?php echo $name; ?>"<?php echo $multi; ?> data-required="<?php echo $attr['required'] ?>" data-type="<?php echo $data_type; ?>"<?php $this->required_html5( $attr ); ?>>

                <?php if ( !empty( $attr['first'] ) ) { ?>
                    <option value=""><?php echo $attr['first']; ?></option>
                <?php } ?>

                <?php
                if ( $attr['options'] && count( $attr['options'] ) > 0 ) {
                    foreach ($attr['options'] as $value => $option) {
                        $current_select = $multiselect ? selected( in_array( $value, $selected ), true, false ) : selected( $selected, $value, false );
                        ?>
                        <option value="<?php echo esc_attr( $value ); ?>"<?php echo $current_select; ?>><?php echo $option; ?></option>
                        <?php
                    }
                }
                ?>
            </select>
            <?php $this->help_text( $attr ); ?>
        </div>
        <?php
    }

    /**
     * Prints a radio field
     *
     * @param array $attr
     * @param int|null $post_id
     */
    function radio( $attr, $post_id, $type, $form_id ) {
        $selected = isset( $attr['selected'] ) ? $attr['selected'] : '';
        if ( $post_id ) {
            $selected = $this->get_meta( $post_id, $attr['name'], $type, true );
        }
        ?>

        <div class="wpuf-fields" data-required="<?php echo $attr['required'] ?>" data-type="radio">

            <?php
            if ( $attr['options'] && count( $attr['options'] ) > 0 ) {
                foreach ($attr['options'] as $value => $option) {
                    ?>

                    <label <?php echo $attr['inline'] == 'yes' ? 'class="wpuf-radio-inline"' : 'class="wpuf-radio-block"'; ?>>
                        <input name="<?php echo $attr['name']; ?>" class="<?php echo 'wpuf_'.$attr['name']. '_'. $form_id; ?>" type="radio" value="<?php echo esc_attr( $value ); ?>"<?php checked( $selected, $value ); ?> />
                        <?php echo $option; ?>
                    </label>
                    <?php
                }
            }
            ?>

            <?php $this->help_text( $attr ); ?>
        </div>

        <?php
    }

    /**
     * Prints a checkbox field
     *
     * @param array $attr
     * @param int|null $post_id
     */
    function checkbox( $attr, $post_id, $type, $form_id ) {
        $selected = isset( $attr['selected'] ) ? $attr['selected'] : array();

        if ( $post_id ) {
            if ( $value = $this->get_meta( $post_id, $attr['name'], $type, true ) ) {
                $selected = explode( self::$separator, $value );
            }
        }
        ?>

        <div class="wpuf-fields" data-required="<?php echo $attr['required'] ?>" data-type="radio">

            <?php
            if ( $attr['options'] && count( $attr['options'] ) > 0 ) {

                foreach ($attr['options'] as $value => $option) {

                    ?>
                    <label <?php echo $attr['inline'] == 'yes' ? 'class="wpuf-checkbox-inline"' : 'class="wpuf-checkbox-block"'; ?>>
                        <input type="checkbox" class="<?php echo 'wpuf_'.$attr['name']. '_'. $form_id; ?>" name="<?php echo $attr['name']; ?>[]" value="<?php echo esc_attr( $value ); ?>"<?php echo in_array( $value, $selected ) ? ' checked="checked"' : ''; ?> />
                        <?php echo $option; ?>
                    </label>
                    <?php
                }
            }
            ?>

            <?php $this->help_text( $attr ); ?>

        </div>

        <?php
    }

    /**
     * Prints a url field
     *
     * @param array $attr
     * @param int|null $post_id
     */
    function url( $attr, $post_id, $type, $form_id ) {

        if ( $post_id ) {
            if ( $this->is_meta( $attr ) ) {
                $value = $this->get_meta( $post_id, $attr['name'], $type, true );
            } else {
                //must be user profile url
                $value = $this->get_user_data( $post_id, $attr['name'] );
            }
        } else {
            $value = $attr['default'];
        }
        ?>

        <div class="wpuf-fields">
            <input id="wpuf-<?php echo $attr['name']; ?>" type="url" class="url <?php echo ' wpuf_'.$attr['name'].'_'.$form_id; ?>" data-required="<?php echo $attr['required'] ?>" data-type="text"<?php $this->required_html5( $attr ); ?> name="<?php echo esc_attr( $attr['name'] ); ?>" placeholder="<?php echo esc_attr( $attr['placeholder'] ); ?>" value="<?php echo esc_attr( $value ) ?>" size="<?php echo esc_attr( $attr['size'] ) ?>" />
            <?php $this->help_text( $attr ); ?>
        </div>

        <?php
    }

    /**
     * Prints a email field
     *
     * @param array $attr
     * @param int|null $post_id
     */
    function email( $attr, $post_id, $type = 'post', $form_id ) {
        if ( $post_id ) {
            if ( $this->is_meta( $attr ) ) {
                $value = $this->get_meta( $post_id, $attr['name'], $type, true );
            } else {
                //must be user email
                $value = $this->get_user_data( $post_id, $attr['name'] );
            }
        } else {
            $value = $attr['default'];
        }
        ?>

        <div class="wpuf-fields">
            <input id="wpuf-<?php echo $attr['name']; ?>" type="email" class="email <?php echo ' wpuf_'.$attr['name'].'_'.$form_id; ?>" data-required="<?php echo $attr['required'] ?>" data-type="email" <?php $this->required_html5( $attr ); ?> name="<?php echo esc_attr( $attr['name'] ); ?>" placeholder="<?php echo esc_attr( $attr['placeholder'] ); ?>" value="<?php echo esc_attr( $value ) ?>" size="<?php echo esc_attr( $attr['size'] ) ?>" />
            <?php $this->help_text( $attr ); ?>
        </div>

        <?php
    }

    /**
     * Prints a email field
     *
     * @param array $attr
     */
    function password( $attr, $post_id, $type, $form_id ) {
        if ( $post_id ) {
            $attr['required'] = 'no';
        }

        $repeat_pass   = ( $attr['repeat_pass'] == 'yes' ) ? true : false;
        $pass_strength = ( $attr['pass_strength'] == 'yes' ) ? true : false;
        ?>

        <div class="wpuf-fields">
            <input id="pass1" type="password" class="password <?php echo ' wpuf_'.$attr['name'].'_'.$form_id; ?>" data-required="<?php echo $attr['required'] ?>" data-type="password"<?php $this->required_html5( $attr ); ?> data-repeat="<?php echo $repeat_pass ? 'true' : 'false'; ?>" name="pass1" placeholder="<?php echo esc_attr( $attr['placeholder'] ); ?>" value="" size="<?php echo esc_attr( $attr['size'] ) ?>" />
            <?php $this->help_text( $attr ); ?>
        </div>

        <?php
        if ( $repeat_pass ) {
            $field_size    = !empty( $attr['width'] ) ? ' field-size-' . $attr['width'] : '';

            echo '</li>';
            echo '<li class="wpuf-el password-repeat ' . $field_size . '" data-label="' . esc_attr( 'Confirm Password', 'wpuf' ) . '">';

            $this->label( array('name' => 'pass2', 'label' => $attr['re_pass_label'], 'required' => $post_id ? 'no' : 'yes') );
            ?>

            <div class="wpuf-fields">
                <input id="pass2" type="password" class="password <?php echo ' wpuf_'.$attr['name'].'_'.$form_id; ?>" data-required="<?php echo $attr['required'] ?>" data-type="confirm_password"<?php $this->required_html5( $attr ); ?> name="pass2" value="" placeholder="<?php echo esc_attr( $attr['placeholder'] ); ?>" size="<?php echo esc_attr( $attr['size'] ) ?>" />
            </div>

            <?php
        }

        if ( $pass_strength ) {
            echo '</li>';
            echo '<li>';

            wp_enqueue_script( 'zxcvbn' );
            wp_enqueue_script( 'password-strength-meter' );
            ?>
            <div class="wpuf-label">
                &nbsp;
            </div>

            <div class="wpuf-fields">
                <div id="pass-strength-result" style="display: block"><?php _e( 'Strength indicator', 'wpuf' ); ?></div>
            </div>

            <script type="text/javascript">
                jQuery(function($) {
                    function check_pass_strength() {
                        var pass1 = $('#pass1').val(),
                            pass2 = $('#pass2').val(),
                            strength;

                        if ( typeof pass2 === undefined ) {
                            pass2 = pass1;
                        }

                        $('#pass-strength-result').removeClass('short bad good strong');
                        if (!pass1) {
                            $('#pass-strength-result').html(pwsL10n.empty);
                            return;
                        }

                        strength = wp.passwordStrength.meter(pass1, wp.passwordStrength.userInputBlacklist(), pass2);

                        switch (strength) {
                            case 2:
                                $('#pass-strength-result').addClass('bad').html(pwsL10n.bad);
                                break;
                            case 3:
                                $('#pass-strength-result').addClass('good').html(pwsL10n.good);
                                break;
                            case 4:
                                $('#pass-strength-result').addClass('strong').html(pwsL10n.strong);
                                break;
                            case 5:
                                $('#pass-strength-result').addClass('short').html(pwsL10n.mismatch);
                                break;
                            default:
                                $('#pass-strength-result').addClass('short').html(pwsL10n['short']);
                        }
                    }

                    $('#pass1').val('').keyup(check_pass_strength);
                    $('#pass2').val('').keyup(check_pass_strength);
                    $('#pass-strength-result').show();
                });
            </script>
            <?php
        }

    }


    function taxnomy_select( $terms, $attr ) {

        $selected           = $terms ? $terms : '';
        $required           = sprintf( 'data-required="%s" data-type="select"', $attr['required'] );
        $taxonomy           = $attr['name'];
        $class              = ' wpuf_'.$attr['name'].'_'.$selected;
        $exclude_type       = isset( $attr['exclude_type'] ) ? $attr['exclude_type'] : 'exclude';
        $exclude            = $attr['exclude'];
        $tax_args           = array(
            'show_option_none' => __( '-- Select --', 'wpuf' ),
            'hierarchical'     => 1,
            'hide_empty'       => 0,
            'orderby'          => isset( $attr['orderby'] ) ? $attr['orderby'] : 'name',
            'order'            => isset( $attr['order'] ) ? $attr['order'] : 'ASC',
            'name'             => $taxonomy . '[]',
            'taxonomy'         => $taxonomy,
            'echo'             => 0,
            'title_li'         => '',
            'class'            => 'cat-ajax '. $taxonomy . $class,
            $exclude_type      => $exclude,
            'selected'         => $selected,
            'depth'            => 1,
            'child_of'         => isset( $attr['parent_cat'] ) ? $attr['parent_cat'] : ''
        );

        $tax_args = apply_filters( 'wpuf_taxonomy_checklist_args', $tax_args );

        $select = wp_dropdown_categories( $tax_args );

        echo str_replace( '<select', '<select ' . $required, $select );
        $attr = array(
            'required'     => $attr['required'],
            'name'         => $attr['name'],
            'exclude_type' => $attr['exclude_type'],
            'exclude'      => $attr['exclude'],
            'orderby'      => $attr['orderby'],
            'order'        => $attr['order'],
            'name'         => $attr['name'],
            //'last_term_id' => isset( $attr['parent_cat'] ) ? $attr['parent_cat'] : '',
            //'term_id'      => $selected
        );
        $attr = apply_filters( 'wpuf_taxonomy_checklist_args', $attr );
        ?>
        <span data-taxonomy=<?php echo json_encode( $attr ); ?>></span>
        <?php
    }

    /**
     * Prints a taxonomy field
     *
     * @param array $attr
     * @param int|null $post_id
     */
    function taxonomy( $attr, $post_id, $form_id ) {

        $exclude_type       = isset( $attr['exclude_type'] ) ? $attr['exclude_type'] : 'exclude';
        $exclude            = $attr['exclude'];
        $taxonomy           = $attr['name'];
        $class              = ' wpuf_'.$attr['name'].'_'.$form_id;
        $current_user       = get_current_user_id();

        $terms = array();
        if ( $post_id && $attr['type'] == 'text' ) {
            $terms = wp_get_post_terms( $post_id, $taxonomy, array('fields' => 'names') );
        } elseif( $post_id ) {
            $terms = wp_get_post_terms( $post_id, $taxonomy, array('fields' => 'ids') );
        }

        if ( ! taxonomy_exists( $taxonomy ) ) {
            echo '<br><div class="wpuf-message">' . __( 'This field is no longer available.', 'wpuf' ) . '</div>';
            return;
        }

        $div_class = 'wpuf_' . $attr['name'] . '_' . $attr['type'] . '_' . $attr['id'] . '_' . $form_id;
        ?>


        <?php if ( $attr['type'] == 'checkbox' ) { ?>
            <div class="wpuf-fields <?php echo $div_class; ?>" data-required="<?php echo esc_attr( $attr['required'] ); ?>" data-type="tax-checkbox">
        <?php } else { ?>
            <div class="wpuf-fields <?php echo $div_class; ?>">
        <?php } ?>

                <?php
                switch ($attr['type']) {
                    case 'ajax':
                        $class = ' wpuf_'.$attr['name'].'_'.$form_id;
                        ?>
                        <div class="category-wrap <?php echo $class; ?>">
                            <?php

                            if ( !count( $terms ) ) {

                                ?>
                                <div id="lvl0" level="0">
                                    <?php $this->taxnomy_select( null, $attr, $form_id ); ?>
                                </div>
                                <?php
                            } else {

                                $level = 0;
                                asort( $terms );
                                $last_term_id = end( $terms );

                                foreach( $terms as $term_id) {
                                    $class = ( $last_term_id != $term_id ) ? 'hasChild' : '';
                                    ?>
                                    <div id="lvl<?php echo $level; ?>" level="<?php echo $level; ?>" >
                                        <?php $this->taxnomy_select( $term_id, $attr ); ?>
                                    </div>
                                <?php
                                    $attr['parent_cat'] = $term_id;
                                    $level++;
                                }
                            }

                        ?>
                        </div>
                        <span class="loading"></span>
                        <?php
                        break;
                    case 'select':
                        $selected = $terms ? $terms[0] : '';
                        $required = sprintf( 'data-required="%s" data-type="select"', $attr['required'] );
                        $tax_args = array(
                            'show_option_none' => isset ( $attr['first'] ) ? $attr['first'] : '--select--',
                            'hierarchical'     => 1,
                            'hide_empty'       => 0,
                            'orderby'          => isset( $attr['orderby'] ) ? $attr['orderby'] : 'name',
                            'order'            => isset( $attr['order'] ) ? $attr['order'] : 'ASC',
                            'name'             => $taxonomy . '[]',
                            'taxonomy'         => $taxonomy,
                            'echo'             => 0,
                            'title_li'         => '',
                            'class'            => $taxonomy . $class,
                            $exclude_type      => $exclude,
                            'selected'         => $selected,
                        );

                        $tax_args = apply_filters( 'wpuf_taxonomy_checklist_args', $tax_args );

                        $select   = wp_dropdown_categories( $tax_args );

                        echo str_replace( '<select', '<select ' . $required, $select );
                        break;

                    case 'multiselect':
                        $selected = $terms ? $terms : array();
                        $required = sprintf( 'data-required="%s" data-type="multiselect"', $attr['required'] );
                        $walker   = new WPUF_Walker_Category_Multi();
                        $tax_args = array(
                            // 'show_option_none' => __( '-- Select --', 'wpuf' ),
                            'hierarchical'     => 1,
                            'hide_empty'       => 0,
                            'orderby'          => isset( $attr['orderby'] ) ? $attr['orderby'] : 'name',
                            'order'            => isset( $attr['order'] ) ? $attr['order'] : 'ASC',
                            'name'             => $taxonomy . '[]',
                            'id'               => 'cat-ajax',
                            'taxonomy'         => $taxonomy,
                            'echo'             => 0,
                            'title_li'         => '',
                            'class'            => $taxonomy . ' multiselect' . $class,
                            $exclude_type      => $exclude,
                            'selected'         => $selected,
                            'walker'           => $walker
                        );

                        $tax_args = apply_filters( 'wpuf_taxonomy_checklist_args', $tax_args );

                        $select   = wp_dropdown_categories( $tax_args );

                        echo str_replace( '<select', '<select multiple="multiple" ' . $required, $select );
                        break;

                    case 'checkbox':
                        wpuf_category_checklist( $post_id, false, $attr, $class );
                        break;

                    case 'text':
                        ?>

                        <input class="textfield<?php echo $this->required_class( $attr ); ?>" id="<?php echo $attr['name']; ?>" type="text" data-required="<?php echo $attr['required'] ?>" data-type="text"<?php $this->required_html5( $attr ); ?> name="<?php echo esc_attr( $attr['name'] ); ?>" value="<?php echo esc_attr( implode( ', ', $terms ) ); ?>" size="40" />

                        <script type="text/javascript">
                            ;(function($) {
                                $(document).ready( function(){
                                        $('#<?php echo $attr['name']; ?>').suggest( wpuf_frontend.ajaxurl + '?action=wpuf-ajax-tag-search&tax=<?php echo $attr['name']; ?>', { delay: 500, minchars: 2, multiple: true, multipleSep: ', ' } );
                                });
                            })(jQuery);
                        </script>

                        <?php
                        break;

                    default:
                        # code...
                        break;
                }
                ?>
            <?php $this->help_text( $attr ); ?>
        </div>


        <?php
    }

    /**
     * Prints a HTML field
     *
     * @param array $attr
     */
    function html( $attr, $form_id ) {
        ?>
        <div class="wpuf-fields <?php echo ' wpuf_'.$attr['name'].'_'.$form_id; ?>">
            <?php echo $attr['html']; ?>
        </div>
        <?php
    }

    /**
     * Prints a image upload field
     *
     * @param array $attr
     * @param int|null $post_id
     */
    function image_upload( $attr, $post_id, $type, $form_id ) {

        $has_featured_image = false;
        $has_images         = false;
        $has_avatar         = false;
        $unique_id          = sprintf( '%s-%d', $attr['name'], $form_id );

        if ( $post_id ) {
            if ( $this->is_meta( $attr ) ) {
                $images = $this->get_meta( $post_id, $attr['name'], $type, false );
                $has_images = true;
            } else {

                if ( $type == 'post' ) {
                    // it's a featured image then
                    $thumb_id = get_post_thumbnail_id( $post_id );

                    if ( $thumb_id ) {
                        $has_featured_image = true;
                        $featured_image = WPUF_Upload::attach_html( $thumb_id, 'featured_image' );
                    }
                } else {
                    // it must be a user avatar
                    $has_avatar = true;
                    $featured_image = get_avatar( $post_id );
                }
            }
        }
        ?>

        <div class="wpuf-fields">
            <div id="wpuf-<?php echo $unique_id; ?>-upload-container">
                <div class="wpuf-attachment-upload-filelist" data-type="file" data-required="<?php echo $attr['required']; ?>">
                    <a id="wpuf-<?php echo $unique_id; ?>-pickfiles" data-form_id="<?php echo $form_id; ?>" class="button file-selector <?php echo ' wpuf_' . $attr['name'] . '_' . $form_id; ?>" href="#"><?php _e( 'Select Image', 'wpuf' ); ?></a>

                    <ul class="wpuf-attachment-list thumbnails">
                        <?php
                        if ( $has_featured_image ) {
                            echo $featured_image;
                        }

                        if ( $has_avatar ) {
                            $avatar = get_user_meta( $post_id, 'user_avatar', true );
                            if ( $avatar ) {
                                echo '<li>'.$featured_image;
                                printf( '<br><a href="#" data-confirm="%s" class="btn btn-danger btn-small wpuf-button button wpuf-delete-avatar">%s</a>', __( 'Are you sure?', 'wpuf' ), __( 'Delete', 'wpuf' ) );
                                echo '</li>';
                            }
                        }

                        if ( $has_images ) {
                            foreach ($images as $attach_id) {
                                echo WPUF_Upload::attach_html( $attach_id, $attr['name'] );
                            }
                        }
                        ?>
                    </ul>
                </div>
            </div><!-- .container -->

            <?php $this->help_text( $attr ); ?>

        </div> <!-- .wpuf-fields -->

        <script type="text/javascript">
            ;(function($) {
                $(document).ready( function(){
                    var uploader = new WPUF_Uploader('wpuf-<?php echo $unique_id; ?>-pickfiles', 'wpuf-<?php echo $unique_id; ?>-upload-container', <?php echo $attr['count']; ?>, '<?php echo $attr['name']; ?>', 'jpg,jpeg,gif,png,bmp', <?php echo $attr['max_size'] ?>);
                    wpuf_plupload_items.push(uploader);
                });
            })(jQuery);
        </script>
    <?php

    }

    /**
     * Prints a section break
     *
     * @param array $attr
     * @param int|null $post_id
     */
    function section_break( $attr, $post_id, $form_id ) {
        ?>
        <div class="wpuf-section-wrap <?php echo ' wpuf_'.$attr['name'].'_'.$attr['id'].'_'.$form_id; ?>">
            <h2 class="wpuf-section-title"><?php echo $attr['label']; ?></h2>
            <div class="wpuf-section-details"><?php echo $attr['description']; ?></div>
        </div>
        <?php
    }

    /**
     * Prints recaptcha field
     *
     * @param array $attr
     */
    public static function recaptcha( $attr, $post_id, $form_id ) {

        if ( $post_id ) {
            return;
        }
        $enable_no_captcha = $enable_invisible_recaptcha = '';
        if ( isset ( $attr['recaptcha_type'] ) ) {
            $enable_invisible_recaptcha = $attr['recaptcha_type'] == 'invisible_recaptcha' ? true : false;
            $enable_no_captcha = $attr['recaptcha_type'] == 'enable_no_captcha' ? true : false;
        }

        if ( $enable_invisible_recaptcha ) { ?>
            <script src="https://www.google.com/recaptcha/api.js?onload=wpufreCaptchaLoaded&render=explicit&hl=en" async defer></script>
            <script>
                jQuery(document).ready(function($) {
                    jQuery('[name="submit"]').removeClass('wpuf-submit-button').addClass('g-recaptcha').attr('data-sitekey', '<?php echo wpuf_get_option( 'recaptcha_public', 'wpuf_general' ); ?>');

                    $(document).on('click','.g-recaptcha', function(e){
                        e.preventDefault();
                        e.stopPropagation();
                        grecaptcha.execute();
                    })
                });

                var wpufreCaptchaLoaded = function() {
                    grecaptcha.render('recaptcha', {
                        'size' : 'invisible',
                        'callback' : wpufRecaptchaCallback
                    });
                    grecaptcha.execute();
                };

                function wpufRecaptchaCallback(token) {
                    jQuery('[name="g-recaptcha-response"]').val(token);
                    jQuery('[name="submit"]').removeClass('g-recaptcha').addClass('wpuf-submit-button');
                }
            </script>
            <!-- <input type="submit" class="g-recaptcha" data-sitekey=<?php echo wpuf_get_option( 'recaptcha_public', 'wpuf_general' ); ?> data-callback="onSubmit"> -->
            <div type="submit" id='recaptcha' class="g-recaptcha" data-sitekey=<?php echo wpuf_get_option( 'recaptcha_public', 'wpuf_general' ); ?> data-callback="onSubmit" data-size="invisible"></div>
        <?php } else { ?>
            <div class="wpuf-fields <?php echo ' wpuf_'.$attr['name'].'_'.$form_id; ?>">
                <?php echo recaptcha_get_html( wpuf_get_option( 'recaptcha_public', 'wpuf_general' ), $enable_no_captcha, null, is_ssl() ); ?>
            </div>
        <?php }
    }


}
