<?php
 
/* Redirect WordPress Logout to Home Page */
	function logout_redirect_home(){
		wp_safe_redirect(home_url());
		exit;
	}
	
	add_action('wp_logout', 'logout_redirect_home');

/* Allow subscribers to see Private posts and pages */
 $subRole = get_role( 'subscriber' ); 
 $subRole->add_cap( 'read_private_posts' );
 $subRole->add_cap( 'read_private_pages' );
 
/* Allow contributors to see Private posts and pages */
 $subRole = get_role( 'contributor' ); 
 $subRole->add_cap( 'read_private_posts' );
 $subRole->add_cap( 'read_private_pages' );

/**
* Redirect buddypress pages to registration page
*/
function kleo_page_template_redirect()
{
    //if not logged in and on a bp page except registration or activation
    if( ! is_user_logged_in() && ! bp_is_blog_page() && ! bp_is_activation_page() && ! bp_is_register_page() ) {
        wp_redirect( home_url( '/login-2/?redirect_to=http%3A%2F%2Fwww.pipertalent.com.au%2F' ) );
        exit();
    }
}
add_action( 'template_redirect', 'kleo_page_template_redirect' );

?>